# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131130024711) do

  create_table "bus_trips", force: true do |t|
    t.integer  "bus_id"
    t.integer  "trip_id"
    t.integer  "bus_order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "buses", force: true do |t|
    t.integer  "unit_number"
    t.integer  "route_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "devices", force: true do |t|
    t.string   "token"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations", force: true do |t|
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.integer  "thing_id"
    t.string   "thing_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "locations", ["thing_id", "thing_type"], name: "index_locations_on_thing_id_and_thing_type"

  create_table "route_stops", force: true do |t|
    t.integer  "route_id"
    t.integer  "stop_id"
    t.integer  "stop_order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "route_trips", force: true do |t|
    t.integer  "route_id"
    t.integer  "trip_id"
    t.integer  "route_order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "routes", force: true do |t|
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "schedule_timepoints", force: true do |t|
    t.integer  "timepoint_id"
    t.integer  "time_order"
    t.integer  "schedule_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "schedules", force: true do |t|
    t.integer  "route_id"
    t.integer  "bus_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stop_trips", force: true do |t|
    t.integer  "stop_id"
    t.integer  "trip_id"
    t.integer  "stop_order"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "stops", force: true do |t|
    t.integer  "serial"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "timepoints", force: true do |t|
    t.integer  "day"
    t.integer  "hour"
    t.integer  "minute"
    t.integer  "event_id"
    t.string   "event_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "timepoints", ["event_id", "event_type"], name: "index_timepoints_on_event_id_and_event_type"

  create_table "trips", force: true do |t|
    t.datetime "start_time"
    t.integer  "duration"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "authentication_token"
    t.boolean  "is_admin",               default: false
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
