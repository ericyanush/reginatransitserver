class CreateBusTrips < ActiveRecord::Migration
  def change
    create_table :bus_trips do |t|
      t.integer :bus_id
      t.integer :trip_id
      t.integer :bus_order

      t.timestamps
    end
  end
end
