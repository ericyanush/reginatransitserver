class CreateScheduleTimepoints < ActiveRecord::Migration
  def change
    create_table :schedule_timepoints do |t|
      t.integer :timepoint_id
      t.integer :time_order
      t.integer :schedule_id

      t.timestamps
    end
  end
end
