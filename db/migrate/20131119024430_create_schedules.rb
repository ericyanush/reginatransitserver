class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.integer :route_id
      t.integer :bus_id

      t.timestamps
    end
  end
end
