class CreateStopTrips < ActiveRecord::Migration
  def change
    create_table :stop_trips do |t|
      t.integer :stop_id
      t.integer :trip_id
      t.integer :stop_order

      t.timestamps
    end
  end
end
