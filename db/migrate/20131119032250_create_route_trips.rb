class CreateRouteTrips < ActiveRecord::Migration
  def change
    create_table :route_trips do |t|
      t.integer :route_id
      t.integer :trip_id
      t.integer :route_order

      t.timestamps
    end
  end
end
