class CreateTrips < ActiveRecord::Migration
  def change
    create_table :trips do |t|
      t.timestamp :start_time
      t.integer :duration
      t.integer :user_id

      t.timestamps
    end
  end
end
