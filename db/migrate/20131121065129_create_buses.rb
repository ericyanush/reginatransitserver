class CreateBuses < ActiveRecord::Migration
  def change
    create_table :buses do |t|
      t.integer :unit_number
      t.integer :route_id

      t.timestamps
    end
  end
end
