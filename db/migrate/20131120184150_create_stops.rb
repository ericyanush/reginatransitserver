class CreateStops < ActiveRecord::Migration
  def change
    create_table :stops do |t|
      t.integer :serial
      t.string :description

      t.timestamps
    end
  end
end
