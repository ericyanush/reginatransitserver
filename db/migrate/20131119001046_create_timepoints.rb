class CreateTimepoints < ActiveRecord::Migration
  def change
    create_table :timepoints do |t|
      t.integer :day
      t.integer :hour
      t.integer :minute
      t.references :event, polymorphic: true, index: true

      t.timestamps
    end
  end
end
