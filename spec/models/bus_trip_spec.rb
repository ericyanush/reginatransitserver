require 'spec_helper'

describe BusTrip do
  it "has a valid factory" do
    FactoryGirl.create(:bus_trip).should be_valid
  end
  it "requires an owning trip" do
    FactoryGirl.build(:bus_trip, trip: nil).should_not be_valid
  end
  it "requires an owning bus" do
    FactoryGirl.build(:bus_trip, bus: nil).should_not be_valid
  end
end
