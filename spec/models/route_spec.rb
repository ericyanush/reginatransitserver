require 'spec_helper'

describe Route do
  it "has a valid factory" do
    FactoryGirl.create(:route).should be_valid
  end
  it "requires a description" do
    FactoryGirl.build(:route, description: nil).should_not be_valid
  end
  it "has a valid set of stops when it has a schedule (counts match)" do
    route = FactoryGirl.create(:route)
    schedule = FactoryGirl.create(:schedule)
    schedule.route = route
    schedule.save
    sttp = FactoryGirl.build(:schedule_timepoint, schedule: schedule, timepoint: FactoryGirl.create(:timepoint))
    sttp.save
    route.should_not be_valid
  end
end
