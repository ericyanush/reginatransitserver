require 'spec_helper'

describe Timepoint do
  it "has a valid factory" do
    FactoryGirl.create(:timepoint).should be_valid
  end
  it "requires a day to be set" do
    FactoryGirl.build(:timepoint, day: nil).should_not be_valid
  end
  it "requires a day in the range of 0..6" do
    FactoryGirl.build(:timepoint, day: -1).should_not be_valid
    FactoryGirl.build(:timepoint, day: 0).should be_valid
    FactoryGirl.build(:timepoint, day: 4).should be_valid
    FactoryGirl.build(:timepoint, day: 6).should be_valid
    FactoryGirl.build(:timepoint, day: 7).should_not be_valid
  end
  it "requires an hour to be set" do
    FactoryGirl.build(:timepoint, hour: nil).should_not be_valid
  end
  it "requires an hour in the range 0..23" do
    FactoryGirl.build(:timepoint, hour: -1).should_not be_valid
    FactoryGirl.build(:timepoint, hour: 0).should be_valid
    FactoryGirl.build(:timepoint, hour: 14).should be_valid
    FactoryGirl.build(:timepoint, hour: 23).should be_valid
    FactoryGirl.build(:timepoint, hour: 24).should_not be_valid
  end
  it "requires a minute to be set" do
    FactoryGirl.build(:timepoint, minute: nil).should_not be_valid
  end
  it "requires a minute in the range 0..59" do
    FactoryGirl.build(:timepoint, minute: -1).should_not be_valid
    FactoryGirl.build(:timepoint, minute: 0).should be_valid
    FactoryGirl.build(:timepoint, minute: 26).should be_valid
    FactoryGirl.build(:timepoint, minute: 59).should be_valid
    FactoryGirl.build(:timepoint, minute: 60).should_not be_valid
  end
end
