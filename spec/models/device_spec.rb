require 'spec_helper'

describe Device do
  it "has a valid factory" do
    FactoryGirl.create(:device).should be_valid
  end
  it "requires a token" do
    FactoryGirl.build(:device, token: nil).should_not be_valid
  end
  it "requires an owning user" do
    FactoryGirl.build(:device, user: nil).should_not be_valid
  end
end
