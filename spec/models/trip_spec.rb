require 'spec_helper'

describe Trip do
  it "has a valid factory" do
    FactoryGirl.create(:trip).should be_valid
  end
  it "requires an owning user" do
    FactoryGirl.build(:trip, user: nil).should_not be_valid
  end
  it "requires a start_time" do
    FactoryGirl.build(:trip, start_time: nil).should_not be_valid
  end
  it "requires a duration" do
    FactoryGirl.build(:trip, duration: nil).should_not be_valid
  end
  it "ensures duration is in the range 1..86400 (24h)" do
    FactoryGirl.build(:trip, duration: 0).should_not be_valid
    FactoryGirl.build(:trip, duration: 1).should be_valid
    FactoryGirl.build(:trip, duration: 7512).should be_valid
    FactoryGirl.build(:trip, duration: 86400).should be_valid
    FactoryGirl.build(:trip, duration: 86401).should_not be_valid
  end
end
