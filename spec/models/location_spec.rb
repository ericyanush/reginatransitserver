require 'spec_helper'

describe Location do
  it "has a valid factory" do
    FactoryGirl.create(:location).should be_valid
  end
  it "ensures longitude is between -180 and 180" do
    FactoryGirl.build(:location, longitude: -180).should be_valid
    FactoryGirl.build(:location, longitude:-180.1).should_not be_valid
    FactoryGirl.build(:location, longitude: 55).should be_valid
    FactoryGirl.build(:location, longitude: 180).should be_valid
    FactoryGirl.build(:location, longitude: 180.1).should_not be_valid
  end
  it "ensures latitude is between -90 and 90" do
    FactoryGirl.build(:location, latitude:-90).should be_valid
    FactoryGirl.build(:location, latitude:-90.1).should_not be_valid
    FactoryGirl.build(:location, latitude:11).should be_valid
    FactoryGirl.build(:location, latitude:90).should be_valid
    FactoryGirl.build(:location, latitude:90.1).should_not be_valid
  end
end
