require 'spec_helper'

describe Stop do
  it "has a valid factory" do
    FactoryGirl.create(:stop).should be_valid
  end
  it "requires a description" do
    FactoryGirl.build(:stop, description: nil).should_not be_valid
    FactoryGirl.build(:stop, description: "").should_not be_valid
  end
  it "requires a serial number" do
    FactoryGirl.build(:stop, serial: nil).should_not be_valid
  end
  it "requires a unique serial number" do
    one = FactoryGirl.create(:stop)
    FactoryGirl.build(:stop, serial: one.serial).should_not be_valid
  end
  it "ensures the serial number is between 0 and 999999" do
    FactoryGirl.build(:stop, serial: -1).should_not be_valid
    FactoryGirl.build(:stop, serial: 0).should be_valid
    FactoryGirl.build(:stop, serial: 123456).should be_valid
    FactoryGirl.build(:stop, serial: 999999).should be_valid
    FactoryGirl.build(:stop, serial: 1000000).should_not be_valid
  end
  it "requires a location" do
    FactoryGirl.build(:stop, location: nil).should_not be_valid
  end
end
