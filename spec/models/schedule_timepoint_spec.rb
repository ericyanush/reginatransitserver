require 'spec_helper'

describe ScheduleTimepoint do
  it "has a valid factory" do
    FactoryGirl.create(:schedule_timepoint).should be_valid
  end
  it "requires an owning schedule" do
    FactoryGirl.build(:schedule_timepoint, schedule: nil).should_not be_valid
  end
  it "requires a timepoint" do
    FactoryGirl.build(:schedule_timepoint, timepoint: nil).should_not be_valid
  end
  it "requires a time_order" do
    FactoryGirl.build(:schedule_timepoint, time_order: nil).should_not be_valid
  end
  it "ensures the time order is greater than or equal to 0" do
    FactoryGirl.build(:schedule_timepoint, time_order: -1).should_not be_valid
    FactoryGirl.build(:schedule_timepoint, time_order: 0).should be_valid
    FactoryGirl.build(:schedule_timepoint, time_order: 55).should be_valid
  end
  it "requires a unique time_order/schedule pair" do
    sched = FactoryGirl.create(:schedule)
    schedtwo = FactoryGirl.create(:schedule)
    one = FactoryGirl.build(:schedule_timepoint, schedule: sched, time_order: 12)
    one.save
    FactoryGirl.build(:schedule_timepoint, schedule: sched, time_order: 12).should_not be_valid
    FactoryGirl.build(:schedule_timepoint, schedule: schedtwo, time_order: 12).should be_valid
  end
end
