require 'spec_helper'

describe User do
  it "has a valid factory" do
    FactoryGirl.create(:user).should be_valid
  end
  it "requires a valid email" do
    FactoryGirl.build(:user,  email: nil).should_not be_valid
    FactoryGirl.build(:user,  email: "thisisnotanemail").should_not be_valid
  end
  it "requires a unique email" do
    user1 = FactoryGirl.create(:user)
    user1.save
    FactoryGirl.build(:user, email: user1.email).should_not be_valid
  end
  it "requires a password" do
    FactoryGirl.build(:user, password: nil).should_not be_valid
  end
end
