require 'spec_helper'

describe Bus do
  it "has a valid factory" do
    FactoryGirl.create(:bus).should be_valid
  end
  it "requires a unit number" do
    FactoryGirl.build(:bus, unit_number: nil).should_not be_valid
  end
  it "requires a unique unit number" do
    one = FactoryGirl.create(:bus)
    FactoryGirl.build(:bus, unit_number: one.unit_number).should_not be_valid
  end
  it "has a unit number in the range 1..9999" do
    FactoryGirl.build(:bus, unit_number: 0).should_not be_valid
    FactoryGirl.build(:bus, unit_number: 1).should be_valid
    FactoryGirl.build(:bus, unit_number: 55).should be_valid
    FactoryGirl.build(:bus, unit_number: 9999).should be_valid
    FactoryGirl.build(:bus, unit_number: 10000).should_not be_valid
  end
end
