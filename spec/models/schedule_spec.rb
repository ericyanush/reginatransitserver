require 'spec_helper'

describe Schedule do
  it "has a valid factory" do
    FactoryGirl.create(:schedule).should be_valid
  end
  it "has an owning route" do
    FactoryGirl.build(:schedule, route: nil).should_not be_valid
  end
end
