require 'spec_helper'

describe RouteTrip do
  it "has a valid factory" do
    FactoryGirl.create(:route_trip).should be_valid
  end
  it "requires an owning trip" do
    FactoryGirl.build(:route_trip, trip: nil).should_not be_valid
  end
  it "requires an owning route" do
    FactoryGirl.build(:route_trip, route: nil).should_not be_valid
  end
  it "requires a route_order" do
    FactoryGirl.build(:route_trip, route_order: nil).should_not be_valid
  end
  it "ensures route_order is greater than or equal to 0" do
    FactoryGirl.build(:route_trip, route_order: -1).should_not be_valid
    FactoryGirl.build(:route_trip, route_order: 0).should be_valid
    FactoryGirl.build(:route_trip, route_order: 12).should be_valid
  end
  it "requires a valid route_order/trip pair" do
    trip = FactoryGirl.create(:trip)
    rtone = FactoryGirl.create(:route)
    rttwo = FactoryGirl.create(:route)
    rtTrpOne = FactoryGirl.build(:route_trip, route: rtone, trip: trip)
    rtTrpOne.save
    FactoryGirl.build(:route_trip, route: rttwo, trip:trip, route_order: rtTrpOne.route_order).should_not be_valid
  end
end
