require 'spec_helper'

describe RouteStop do
  it "has a valid factory" do
    FactoryGirl.create(:route_stop).should be_valid
  end
  it "requires an owning route" do
    FactoryGirl.build(:route_stop, route: nil).should_not be_valid
  end
  it "requires an owning stop" do
    FactoryGirl.build(:route_stop, stop: nil).should_not be_valid
  end
  it "requires a stop_order"  do
    FactoryGirl.build(:route_stop, stop_order: nil).should_not be_valid
  end
  it "ensures stop_order is greater than or equal to 0" do
    FactoryGirl.build(:route_stop, stop_order: -1).should_not be_valid
    FactoryGirl.build(:route_stop, stop_order: 0).should be_valid
    FactoryGirl.build(:route_stop, stop_order: 41).should be_valid
  end
  it "requires a unique stop_order/route combination" do
    route = FactoryGirl.create(:route)
    stpone = FactoryGirl.create(:stop)
    stptwo = FactoryGirl.create(:stop)
    one = FactoryGirl.build(:route_stop, route: route, stop: stpone, stop_order: 3)
    one.save
    FactoryGirl.build(:route_stop, route: route, stop: stptwo, stop_order: 3).should_not be_valid
  end

end
