require 'spec_helper'

describe StopTrip do
  it "has a valid factory" do
    FactoryGirl.create(:stop_trip).should be_valid
  end
  it "requires an owning stop" do
    FactoryGirl.build(:stop_trip, stop: nil).should_not be_valid
  end
  it "requires an owning trip" do
    FactoryGirl.build(:stop_trip, trip: nil).should_not be_valid
  end
  it "requires a stop_order" do
    FactoryGirl.build(:stop_trip, stop_order: nil).should_not be_valid
  end
  it "ensures stop_order is greater than or equal to 0" do
    FactoryGirl.build(:stop_trip, stop_order: -1).should_not be_valid
    FactoryGirl.build(:stop_trip, stop_order: 0).should be_valid
    FactoryGirl.build(:stop_trip, stop_order: 55).should be_valid
  end
  it "requires a valid trip/stop_order pair" do
    trip = FactoryGirl.create(:trip)
    stpone = FactoryGirl.create(:stop)
    stptwo = FactoryGirl.create(:stop)
    stpTrpOne = FactoryGirl.build(:stop_trip, trip: trip, stop: stpone)
    stpTrpOne.save
    FactoryGirl.build(:stop_trip, trip: trip, stop: stptwo, stop_order: stpTrpOne.stop_order).should_not be_valid
  end
end
