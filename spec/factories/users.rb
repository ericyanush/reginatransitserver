# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password { (8...50).map{ ('a'..'z').to_a[rand(26)] }.join }
  end
end
