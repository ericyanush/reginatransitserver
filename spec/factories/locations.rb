# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :location do
    latitude { rand(-90..90) }
    longitude { rand(-180..180) }
  end
end
