# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :route_trip do
    trip { FactoryGirl.create(:trip) }
    route { FactoryGirl.create(:route) }
    route_order { rand(0..9999) }
  end
end
