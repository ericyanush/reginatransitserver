# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :stop_trip do
    stop { FactoryGirl.create(:stop) }
    trip { FactoryGirl.create(:trip) }
    stop_order { rand(0..9999) }
  end
end
