# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bus do
    unit_number { rand(1..9999)}
  end
end
