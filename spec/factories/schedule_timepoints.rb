# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :schedule_timepoint do
    timepoint { FactoryGirl.create(:timepoint) }
    schedule { FactoryGirl.create(:schedule) }
    time_order { rand(0..500) }
  end
end
