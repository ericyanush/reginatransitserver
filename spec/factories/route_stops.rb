# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :route_stop do
    route { FactoryGirl.create(:route) }
    stop { FactoryGirl.create(:stop) }
    stop_order { rand(0..9999) }
  end
end
