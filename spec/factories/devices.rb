# Read about factories at https://github.com/thoughtbot/factory_girl
require 'securerandom'

FactoryGirl.define do
  factory :device do
    token { SecureRandom.hex(64) }
    user { FactoryGirl.create(:user) }
  end
end
