# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :trip do
    user { FactoryGirl.create(:user) }
    start_time { Time::now }
    duration { rand(1..86400) }
  end
end
