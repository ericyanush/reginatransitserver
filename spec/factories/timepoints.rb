# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :timepoint do
    day    { rand(0..6) }
    hour   { rand(0..23) }
    minute { rand(0..59) }
  end
end
