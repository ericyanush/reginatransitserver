# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :stop do
    serial { rand(0..999999) }
    description { Faker::Address.street_name + " & " + Faker::Address.street_name }
    location { FactoryGirl.create(:location) }
  end
end
