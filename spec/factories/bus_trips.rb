# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :bus_trip do
    bus { FactoryGirl.create(:bus) }
    trip { FactoryGirl.create(:trip) }
    bus_order { rand(0..9999) }
  end
end
