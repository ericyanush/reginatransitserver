# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :route do
    description  { Faker::Lorem.word }
  end
end
