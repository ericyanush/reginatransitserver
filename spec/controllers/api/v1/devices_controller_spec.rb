require 'spec_helper'

describe API::V1::DevicesController do
  describe "GET #index"  do
    context "logged in user" do
      it "should return a list of all devices for the logged in user" do
        device = FactoryGirl.create(:device)
        user = device.user
        get :index, :format => :json,
            authentication_token: user.authentication_token
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('devices')
        body['devices'].count eq (Device.where(user_id: user).count)
      end
      it "should return each device with only id, and token" do
        device = FactoryGirl.create(:device)
        user = device.user
        get :index, :format => :json,
            authentication_token: user.authentication_token
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('devices')
        body['devices'][0].keys.count.should eq(2)
        body['devices'][0].keys.should include('id')
        body['devices'][0].keys.should include('token')
      end
    end
    context "not logged in" do
      it "should return a 401 unauthorized" do
        device = FactoryGirl.create(:device)
        user = device.user
        get :index, :format => :json
        response.status.should eq(401)
        body = JSON.parse(response.body)
        body.should include('errors')
        end
    end
  end

  describe "POST #create" do
    context "logged in user" do
      context "with valid parameters" do
        it "should create a new device" do
          user = FactoryGirl.create(:user)
          device = FactoryGirl.attributes_for(:device)
          expect {
            post :create, :format => :json, device: device,
                 authentication_token: user.authentication_token
          }.to change(Device, :count).by(1)
          response.status.should eq(201)
        end
        it "should return the Device json object" do
          user = FactoryGirl.create(:user)
          device = FactoryGirl.attributes_for(:device)
          post :create, :format => :json, device: device,
               authentication_token: user.authentication_token
          response.status.should eq(201)
          body = JSON.parse(response.body)
          body.should include('device')
        end
        it "should return the device with only id and token" do
          user = FactoryGirl.create(:user)
          device = FactoryGirl.attributes_for(:device)
          post :create, :format => :json, device: device,
               authentication_token: user.authentication_token
          response.status.should eq(201)
          body = JSON.parse(response.body)
          body.should include('device')
          body['device'].keys.count.should eq(2)
          body['device'].keys.should include('id')
          body['device'].keys.should include('token')
        end
      end
      context "with invalid parameters" do
        it "should not create a device object" do
          user = FactoryGirl.create(:user)
          device = FactoryGirl.attributes_for(:device)
          expect {
            post :create, :format => :json, device: { token: nil},
                 authentication_token: user.authentication_token
          }.to_not change(Device, :count)
        end
        it "should return a json object with errors" do
          user = FactoryGirl.create(:user)
          device = FactoryGirl.attributes_for(:device)
          post :create, :format => :json, device: { token: nil},
               authentication_token: user.authentication_token
          body = JSON.parse(response.body)
          body.should include('errors')
        end
      end
    end
    context "not logged in" do
      it "returns unauthorized if no user is logged in (auth token)" do
        device = FactoryGirl.attributes_for(:device)
        post :create, :format => :json, device: device
        response.status.should eq(401)
      end
      it "does not create a device" do
        device = FactoryGirl.attributes_for(:device)
        expect {
          post :create, :format => :json, device: device
        }.to_not change(Device, :count)
        response.status.should eq(401)
      end
    end
  end

  describe "GET #show" do
    context "logged in" do
      context "with valid id"  do
        it "returns a json object of the device" do
          device = FactoryGirl.create(:device)
          user = device.user
          get :show, :format => :json, id: device.id,
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body.should include('device')
        end
        it "should return the device with only id, and token" do
          device = FactoryGirl.create(:device)
          user = device.user
          get :show, :format => :json, id: device.id,
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['device'].keys.count.should eq(2)
          body['device'].keys.should include('id')
          body['device'].keys.should include('token')
        end
      end
      context "with invalid id"  do
        it "should return a not found (404)" do
          if Device.exists?(5)
            Device.find(5).destroy
          end
          user = FactoryGirl.create(:user)
          get :show, id: 5,
              authentication_token: user.authentication_token
          response.status.should eq(404)
        end
      end
    end
    context "not logged in" do
      it "should return 401 not authorized" do
        device = FactoryGirl.create(:device)
        get :show, :format => :json, id: device.id
        response.status.should eq(401)
      end
      it "should return a 404 not found if the device does not match the user" do
        device = FactoryGirl.create(:device)
        user = FactoryGirl.create(:user)
        get :show,  :format => :json, id: device.id,
            authentication_token: user.authentication_token
      end
    end
  end

  describe "PUT #update" do
    context "logged in user" do
      context "with valid parameters" do
        it "should return an updated json device object" do
          device = FactoryGirl.create(:device)
          user = device.user
          put :update, id: device.id, :format => :json, device: { token: "test token" },
              authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('device')
          body['device']['token'].should eq ("test token")
        end
        it "should return the bus with only id, and token" do
          device = FactoryGirl.create(:device)
          user = device.user
          post :update, :format => :json, id: device.id, device: { token: "test token" },
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body.should include('device')
          body['device'].keys.count.should eq(2)
          body['device'].keys.should include('id')
          body['device'].keys.should include('token')
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found if the device id does not exist" do
          device = FactoryGirl.create(:device)
          user = device.user
          if Device.exists?(5)
            Device.find(5).destroy
          end
          put :update, id: 5, device: { token: "test token" },
              authentication_token: user.authentication_token
          response.status.should eq(404)
        end
        it "should return errors when invalid attributes are supplied" do
          device = FactoryGirl.create(:device)
          user = device.user
          put :update, id: device.id, device: { token: nil },
              authentication_token: user.authentication_token
          response.status.should eq(400)
          body = JSON.parse(response.body)
          body.should include('errors')
        end
      end
    end
    context "not logged in" do
      it "should return 401 unauthorized" do
        device = FactoryGirl.create(:device)
        expect {
          put :update, id: device.id, :format => :json, device: { token: "test token" }
        }.to_not change(device, :token)
        response.status.should eq(401)
      end
      it "should return 404 not found when the device doesn't belong to the user" do
        device = FactoryGirl.create(:device)
        user = FactoryGirl.create(:user)
        expect {
          put :update, id: device.id, :format => :json, device: { token: "test token" },
              authentication_token: user.authentication_token
        }.to_not change(device, :token)
        response.status.should eq(404)
      end
    end
  end

  describe "DELETE #destroy" do
    context "logged in" do
      context "with valid parameters" do
        it "should return a copy of the device that was deleted" do
          device = FactoryGirl.create(:device)
          user = device.user
          delete :destroy, id: device,
                 authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('device')
        end
        it "should return the device with only id, and token" do
          device = FactoryGirl.create(:device)
          user = device.user
          delete :destroy, :format => :json, id: device,
                 authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body.should include('device')
          body['device'].keys.count.should eq(2)
          body['device'].keys.should include('id')
          body['device'].keys.should include('token')
        end
        it "should remove the device from the database" do
          device = FactoryGirl.create(:device)
          user = device.user
          expect {
            delete :destroy, id: device,
                   authentication_token: user.authentication_token
          }.to change(Device, :count).by(-1)
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found if the user does not have a device with that id" do
          user = FactoryGirl.create(:user)
          if Device.exists?(5)
            Device.find(5).destroy
          end
          delete :destroy, id: 5,
                 authentication_token: user.authentication_token
          response.status.should eq(404)
        end
      end
    end
    context "not logged in" do
      it "should return 401 unauthorized" do
        device = FactoryGirl.create(:device)
        delete :destroy, id: device
        response.status.should eq(401)
      end
      it "should not modify the database" do
        device = FactoryGirl.create(:device)
        expect {
          delete :destroy, id: device
        }.to_not change(Device, :count)
      end
      it "should return 404 not found if the user does not match the device" do
        device = FactoryGirl.create(:device)
        user = FactoryGirl.create(:user)
        expect {
          delete :destroy, id: device,
                 authentication_token: user.authentication_token
        }.to_not change(Device, :count)
        response.status.should eq(404)
      end
    end
  end
end
