require 'spec_helper'

describe API::V1::BusesController do
  describe "GET #index"  do
    it "should return a list of all buses" do
      FactoryGirl.create(:bus)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body.should include('buses')
      body['buses'].count eq (Bus.count)
    end
    it "should return each bus with only id, unit_number, schedule, route, and location" do
      bus = FactoryGirl.create(:bus)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body.should include('buses')
      body['buses'][0].keys.count.should eq(5)
      body['buses'][0].keys.should include('id')
      body['buses'][0].keys.should include('unit_number')
      body['buses'][0].keys.should include('schedule_id')
      body['buses'][0].keys.should include('route_id')
      body['buses'][0].keys.should include('location')
    end
    it "should only embed schedule id" do
      bus = FactoryGirl.create(:bus)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body['buses'][0]['schedule_id'].kind_of?(Hash).should_not eq(true)
    end
    it "should only embed route_id" do
      bus = FactoryGirl.create(:bus)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body['buses'][0]['route_id'].kind_of?(Hash).should_not eq(true)
    end
    it "should return location with only latitude and longitude" do
      bus = FactoryGirl.create(:bus)
      bus.location = FactoryGirl.create(:location)
      bus.save
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body['buses'][0]['location'].keys.count.should eq(2)
      body['buses'][0]['location'].should include('latitude')
      body['buses'][0]['location'].should include('longitude')
    end
  end

  describe "POST #create" do
    context "logged in as admin user" do
      context "with valid parameters" do
        it "should create a new bus" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.attributes_for(:bus)
          expect {
            post :create, :format => :json, bus: bus,
                 authentication_token: user.authentication_token
          }.to change(Bus, :count).by(1)
          response.status.should eq(201)
        end
        it "should return the Bus json object" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.attributes_for(:bus)
          post :create, :format => :json, bus: bus,
               authentication_token: user.authentication_token
          response.status.should eq(201)
          body = JSON.parse(response.body)
          body.should include('bus')
        end
        it "should return each bus with only id, unit_number, schedule, route, and location" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.attributes_for(:bus)
          post :create, :format => :json, bus: bus,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body.should include('bus')
          body['bus'].keys.count.should eq(5)
          body['bus'].keys.should include('id')
          body['bus'].keys.should include('unit_number')
          body['bus'].keys.should include('schedule_id')
          body['bus'].keys.should include('route_id')
          body['bus'].keys.should include('location')
        end
        it "should only embed schedule id" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.attributes_for(:bus)
          post :create, :format => :json, bus: bus,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['bus']['schedule_id'].kind_of?(Hash).should_not eq(true)
        end
        it "should only embed route_id" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.attributes_for(:bus)
          post :create, :format => :json, bus: bus,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['bus']['route_id'].kind_of?(Hash).should_not eq(true)
        end
        it "should return location with only latitude and longitude" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.attributes_for(:bus)
          post :create, :format => :json, bus: {unit_number: bus[:unit_number],
                                                location_attributes: {latitude: -5, longitude: 14.22}},
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['bus']['location'].keys.count.should eq(2)
          body['bus']['location'].should include('latitude')
          body['bus']['location'].should include('longitude')
        end
      end
      context "with invalid parameters" do
        it "should not create a route object" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.attributes_for(:bus)
          expect {
            post :create, :format => :json, bus: { unit_number: nil,
                                                  location_attributes: { latitude: "st", longitude: -999 }},
                 authentication_token: user.authentication_token
          }.to_not change(bus, :count)
        end
        it "should return a json object with errors" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.attributes_for(:bus)
          post :create, :format => :json, bus: { unit_number: nil,
                                                   location_attributes: { latitude: "st", longitude: -999 }},
                 authentication_token: user.authentication_token
          body = JSON.parse(response.body)
          body.should include('errors')
        end
      end
    end
    context "not logged in as admin" do
      it "returns unauthorized if no user is logged in (auth token)" do
        bus = FactoryGirl.attributes_for(:bus)
        post :create, :format => :json, bus: bus
        response.status.should eq(401)
      end
      it "returns forbidden if the user logged in is not admin" do
        user = FactoryGirl.create(:user)
        bus = FactoryGirl.attributes_for(:bus)
        post :create, :format => :json, bus: bus,
             authentication_token: user.authentication_token
        response.status.should eq(403)
      end
      it "does not create a bus" do
        user = FactoryGirl.create(:user)
        bus = FactoryGirl.attributes_for(:bus)
        expect {
          post :create, :format => :json, bus: bus,
               authentication_token: user.authentication_token
        }.to_not change(Bus, :count)
        response.status.should eq(403)
      end
    end
  end

  describe "GET #show" do
    context "with valid id"  do
      it "returns a json object of the route" do
        bus = FactoryGirl.create(:bus)
        get :show, :format => :json, id: bus.id
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('bus')
      end
      it "should return the bus with only id, unit_number, schedule, route, and location" do
        bus = FactoryGirl.create(:bus)
        get :show, :format => :json, id: bus.id
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('bus')
        body['bus'].keys.count.should eq(5)
        body['bus'].keys.should include('id')
        body['bus'].keys.should include('unit_number')
        body['bus'].keys.should include('schedule_id')
        body['bus'].keys.should include('route_id')
        body['bus'].keys.should include('location')
      end
      it "should only embed schedule id" do
        bus = FactoryGirl.create(:bus)
        schedule = FactoryGirl.create(:schedule)
        bus.schedule = schedule
        bus.save
        get :show, :format => :json, id: bus.id
        response.should be_success
        body = JSON.parse(response.body)
        body['bus']['schedule_id'].kind_of?(Hash).should_not eq(true)
        body['bus']['schedule_id'].should eq(schedule.id)
      end
      it "should only embed route_id" do
        bus = FactoryGirl.create(:bus)
        route = FactoryGirl.create(:route)
        bus.route = route
        bus.save
        get :show, :format => :json, id: bus.id
        response.should be_success
        body = JSON.parse(response.body)
        body['bus']['route_id'].kind_of?(Hash).should_not eq(true)
        body['bus']['route_id'].should eq(route.id)
      end
      it "should return location with only latitude and longitude" do
        bus = FactoryGirl.create(:bus)
        bus.location = FactoryGirl.create(:location)
        bus.save
        get :show, :format => :json, id: bus.id
        response.should be_success
        body = JSON.parse(response.body)
        body['bus']['location'].keys.count.should eq(2)
        body['bus']['location'].should include('latitude')
        body['bus']['location'].should include('longitude')
      end
    end
    context "with invalid id"  do
      it "should return a not found (404)" do
        if Bus.exists?(5)
          Bus.find(5).destroy
        end
        get :show, id: 5
        response.status.should eq(404)
      end
    end
  end

  describe "PUT #update" do
    context "logged in as admin" do
      context "with valid parameters" do
        it "should return an updated json schedule object" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          put :update, id: bus.id, :format => :json, bus: { unit_number: bus.unit_number+1 },
            authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('bus')
          body['bus']['unit_number'].should eq (bus.unit_number+1)
        end
        it "should return the bus with only id, unit_number, schedule, route, and location" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          post :update, :format => :json, id: bus.id, bus: { unit_number: bus.unit_number+1,
                                                 location_attributes:{latitude: 12, longitude: -11}, },
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body.should include('bus')
          body['bus'].keys.count.should eq(5)
          body['bus'].keys.should include('id')
          body['bus'].keys.should include('unit_number')
          body['bus'].keys.should include('schedule_id')
          body['bus'].keys.should include('route_id')
          body['bus'].keys.should include('location')
        end
        it "should only embed schedule id" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          post :update, :format => :json, id: bus.id, bus: { unit_number: 1234},
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['bus']['schedule_id'].kind_of?(Hash).should_not eq(true)
        end
        it "should only embed route_id" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          post :update, :format => :json, id: bus.id, bus: { unit_number: 1234},
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['bus']['route_id'].kind_of?(Hash).should_not eq(true)
        end
        it "should return location with only latitude and longitude" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          post :update, :format => :json, id: bus.id, bus: { unit_number: bus.unit_number+1,
                                                 location_attributes:{latitude: 12, longitude: -11}, },
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['bus']['location'].keys.count.should eq(2)
          body['bus']['location'].should include('latitude')
          body['bus']['location'].should include('longitude')
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found if the schedule id does not exist" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          if Bus.exists?(5)
            Bus.find(5).destroy
          end
          put :update, id: 5, bus: { unit_number: 111 }, authentication_token: user.authentication_token
          response.status.should eq(404)
        end
        it "should return errors when invalid attributes are supplied" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          put :update, id: bus.id, bus: { unit_number: nil,
                                          location_attributes: {latitude: "sdf", longitude: -11111} },
              authentication_token: user.authentication_token
          response.status.should eq(400)
          body = JSON.parse(response.body)
          body.should include('errors')
        end
      end
    end
    context "not logged in as admin" do
      it "should return 403 forbiden" do
        user = FactoryGirl.create(:user)
        bus = FactoryGirl.create(:bus)
        expect {
          put :update, id: bus.id, :format => :json, bus: { unit_number: 1233 },
              authentication_token: user.authentication_token
        }.to_not change(bus, :unit_number)
        response.status.should eq(403)
      end
    end
  end

  describe "DELETE #destroy" do
    context "logged in as admin" do
      context "with valid parameters" do
        it "should return a copy of the bus that was deleted" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          delete :destroy, id: bus, authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('bus')
        end
        it "should return the bus with only id, unit_number, schedule, route, and location" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          delete :destroy, :format => :json, id: bus.id,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body.should include('bus')
          body['bus'].keys.count.should eq(5)
          body['bus'].keys.should include('id')
          body['bus'].keys.should include('unit_number')
          body['bus'].keys.should include('schedule_id')
          body['bus'].keys.should include('route_id')
          body['bus'].keys.should include('location')
        end
        it "should only embed schedule id" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          delete :destroy, :format => :json, id: bus.id,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['bus']['schedule_id'].kind_of?(Hash).should_not eq(true)
        end
        it "should only embed route_id" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          delete :destroy, :format => :json, id: bus.id,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['bus']['route_id'].kind_of?(Hash).should_not eq(true)
        end
        it "should return location with only latitude and longitude" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          bus.location = FactoryGirl.create(:location)
          bus.save
          delete :destroy, :format => :json, id: bus.id,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['bus']['location'].keys.count.should eq(2)
          body['bus']['location'].should include('latitude')
          body['bus']['location'].should include('longitude')
        end
        it "should remove the bus from the database" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          expect {
            delete :destroy, id: bus, authentication_token: user.authentication_token
          }.to change(Bus, :count).by(-1)
        end
        it "should remove the linked location from the database" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          bus = FactoryGirl.create(:bus)
          bus.location = FactoryGirl.create(:location)
          bus.save
          expect {
            delete :destroy, id: bus, authentication_token: user.authentication_token
          }.to change(Location, :count).by(-1)
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found it the stop id does not exist" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          if Bus.exists?(5)
            Bus.find(5).destroy
          end
          delete :destroy, id: 5, authentication_token: user.authentication_token
          response.status.should eq(404)
        end
      end
    end
    context "not logged in as admin" do
      it "should return 403 forbidden" do
        user = FactoryGirl.create(:user)
        bus = FactoryGirl.create(:bus)
        delete :destroy, id: bus, authentication_token: user.authentication_token
        response.status.should eq(403)
      end
      it "should not modify the database" do
        user = FactoryGirl.create(:user)
        bus = FactoryGirl.create(:bus)
        expect {
          delete :destroy, id: bus, authentication_token: user.authentication_token
        }.to_not change(Bus, :count)
      end
    end
  end
end
