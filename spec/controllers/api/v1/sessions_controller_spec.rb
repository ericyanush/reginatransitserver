require 'spec_helper'

describe API::V1::SessionsController do

  describe "POST #create" do
    context "with valid parameters" do
      it "should return a json object including the auth token and user" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        post :create, user: { email: user.email, password: user.password }, :format => :json
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('authentication_token')
        body.should include('user')
        body['user'].should include('email')
      end
      it "should only return email attr for user" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        post :create, user: { email: user.email, password: user.password }, :format => :json
        response.should be_success
        body = JSON.parse(response.body)
        body['user'].keys.count.should eq(1)
        body['user'].should include('email')
      end
    end
    context "with invalid parameters" do
      it "should return a json object with errors" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.attributes_for(:user)
        post :create, user: user, :format => :json
        body = JSON.parse(response.body)
        body['success'].should eq(false)
        body.should include('errors')
      end
      it "should return a status of unauthorized (401)" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.attributes_for(:user)
        post :create, user: user, :format => :json
        response.status.should eq(401)
      end
    end

  end

  describe "DELETE #destroy" do
    context "with valid parameters" do
      it "should return a json object of the user" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        post :create, user: { email: user.email, password: user.password }, :format => :json
        delete :destroy, authentication_token: user.authentication_token, :format => :json
        body = JSON.parse(response.body)
        body.should include('user')
      end
      it "should return a user object with only email field" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.create(:user)
        post :create, user: { email: user.email, password: user.password }, :format => :json
        delete :destroy, authentication_token: user.authentication_token, :format => :json
        body = JSON.parse(response.body)
        body.should include('user')
        body['user'].keys.count.should eq(1)
        body['user'].should include('email')
      end
    end
    context "with invalid parameters" do
      it "should return a json object with errors" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        delete :destroy, :format => :json
        body = JSON.parse(response.body)
        body.should include('errors')
        body.should include('success')
        body['success'].should eq(false)
      end
    end

  end

end
