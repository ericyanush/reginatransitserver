require 'spec_helper'

describe API::V1::StopsController do

  describe "GET #index"  do
    it "should return a list of all stops" do
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body.should include('stops')
      body['stops'].count eq (Stop.count)
    end
    it "should return each stop with only id, serial, description and location" do
      FactoryGirl.create(:stop)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body.should include('stops')
      body['stops'][0].keys.count.should eq(4)
      body['stops'][0].keys.should include('id')
      body['stops'][0].keys.should include('serial')
      body['stops'][0].keys.should include('description')
      body['stops'][0].keys.should include('location')
    end
    it "should return a stop's location with only latitude and longitude" do
      FactoryGirl.create(:stop)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body.should include('stops')
      body['stops'][0]['location'].keys.count.should eq(2)
      body['stops'][0]['location'].should include('latitude')
      body['stops'][0]['location'].should include('longitude')
    end
  end

  describe "POST #create" do
    it "returns unauthorized if no user is logged in (auth token)" do
      user = FactoryGirl.create(:user)
      stop = FactoryGirl.attributes_for(:stop)
      location = FactoryGirl.attributes_for(:location)
      post :create, :format => :json, stop: { serial: stop[:serial], description: stop[:description],
        location_attributes: location}
      response.status.should eq(401)
    end
    it "returns forbidden if the user logged in is not admin" do
      user = FactoryGirl.create(:user)
      stop = FactoryGirl.attributes_for(:stop)
      location = FactoryGirl.attributes_for(:location)
      post :create, :format => :json, stop: { serial: stop[:serial], description: stop[:description],
        location_attributes: location}, authentication_token: user.authentication_token
      response.status.should eq(403)
    end
    context "with valid parameters" do
      it "should create a new stop" do
        user = FactoryGirl.build(:user, is_admin: true)
        user.save
        stop = FactoryGirl.attributes_for(:stop)
        location = FactoryGirl.attributes_for(:location)
        expect {
          post :create, :format => :json, stop: { serial: stop[:serial], description: stop[:description],
                                                location_attributes: location}, authentication_token: user.authentication_token
        }.to change(Stop, :count).by(1)
      end
      it "should return the stop json object" do
        user = FactoryGirl.build(:user, is_admin: true)
        user.save
        stop = FactoryGirl.attributes_for(:stop)
        location = FactoryGirl.attributes_for(:location)
        post :create, :format => :json, stop: { serial: stop[:serial], description: stop[:description],
                                                location_attributes: location}, authentication_token: user.authentication_token
        response.status.should eq(201)
        body = JSON.parse(response.body)
        body.should include('stop')
      end
      it "should return stop object with only serial, description, id, location(latitude, longitude" do
        user = FactoryGirl.build(:user, is_admin: true)
        user.save
        stop = FactoryGirl.attributes_for(:stop)
        location = FactoryGirl.attributes_for(:location)
        post :create, :format => :json, stop: { serial: stop[:serial], description: stop[:description],
                                                location_attributes: location}, authentication_token: user.authentication_token
        response.status.should eq(201)
        body = JSON.parse(response.body)
        body['stop'].keys.count.should eq(4)
        body['stop'].keys.should include('serial')
        body['stop'].keys.should include('description')
        body['stop'].keys.should include('id')
        body['stop'].keys.should include('location')
        body['stop']['location'].keys.count.should eq(2)
        body['stop']['location'].keys.should include('latitude')
        body['stop']['location'].keys.should include('longitude')
      end
    end
    context "with invalid parameters" do
      it "should not create a stop object" do
        user = FactoryGirl.build(:user, is_admin: true)
        user.save
        stop = FactoryGirl.attributes_for(:stop)
        expect {
          post :create, :format => :json, stop: { serial: stop[:serial], description: stop[:description]}, authentication_token: user.authentication_token
        }.to_not change(Stop, :count).by(1)
      end
      it "should return a json object with errors" do
        user = FactoryGirl.build(:user, is_admin: true)
        user.save
        stop = FactoryGirl.attributes_for(:stop)
        post :create, :format => :json, stop: { serial: stop[:serial], description: stop[:description]},
             authentication_token: user.authentication_token
        response.status.should eq(400)
        body = JSON.parse(response.body)
        body.should include('errors')
      end
    end

  end

  describe "GET #show" do
    context "with valid id"  do
      it "returns a json object of the stop" do
        stop = FactoryGirl.create(:stop)
        get :show, id: stop.id
        response.status.should eq(200)
        body = JSON.parse(response.body)
        body.should include('stop')
      end
      it "should return a stop with only id, serial, description, location" do
        stop = FactoryGirl.create(:stop)
        get :show, id: stop.id
        response.status.should eq(200)
        body = JSON.parse(response.body)
        body['stop'].keys.count.should eq(4)
        body['stop'].should include('id')
        body['stop'].should include('serial')
        body['stop'].should include('description')
        body['stop'].should include('location')
      end
      it "should only return a location with latitude and longitude" do
        stop = FactoryGirl.create(:stop)
        get :show, id: stop.id
        response.status.should eq(200)
        body = JSON.parse(response.body)
        body['stop'].should include('location')
        body['stop']['location'].keys.count.should eq(2)
        body['stop']['location'].should include('latitude')
        body['stop']['location'].should include('longitude')
      end
    end
    context "with invalid id"  do
      it "should return a not found (404)" do
        if Stop.exists?(5)
          Stop.find(5).destroy
        end
        get :show, id: 5
        response.status.should eq(404)
      end
    end
  end

  describe "PUT #update" do
    context "logged in as admin" do
      context "with valid parameters" do
        it "should return an updated json stop object" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          stop = FactoryGirl.create(:stop)
          put :update, id: stop, stop: { description: stop['description'],
                                         serial: stop['serial']+1},
              authentication_token: user.authentication_token

          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('stop')
          body['stop']['serial'].should eq(stop.serial+1)
        end
        it "should only return id, description, serial and location for stop" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          stop = FactoryGirl.create(:stop)
          put :update, id: stop, stop: {description: stop['description'], serial: stop['serial']+1}, authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('stop')
          body['stop'].keys.count.should eq(4)
          body['stop'].should include('id')
          body['stop'].should include('description')
          body['stop'].should include('serial')
          body['stop'].should include('location')
        end
        it "should only return latitude and longitude for the stop location" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          stop = FactoryGirl.create(:stop)
          put :update, id: stop, stop: {description: stop['description'], serial: stop['serial']+1}, authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['stop'].should include('location')
          body['stop']['location'].keys.count.should eq(2)
          body['stop']['location'].should include('latitude')
          body['stop']['location'].should include('longitude')
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found if the stop id does not exist" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          if Stop.exists?(5)
            Stop.find(5).destroy
          end
          put :update, id: 5, stop: {serial: 1234, description: "some", location_attributes: {latitude: 1.0, longitude: 2.0}}, authentication_token: user.authentication_token
          response.status.should eq(404)
        end
        it "should return errors when invalid attributes are supplied" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          stop = FactoryGirl.create(:stop)
          put :update, id: stop, stop: {serial: "garbage", description: nil, location_attributes: {latitude: 9999, longitude: 9999}}, authentication_token: user.authentication_token
          response.status.should eq(400)
          body = JSON.parse(response.body)
          body.should include('errors')
        end
      end
    end
    context "not logged in as admin" do
      it "should return 403 forbiden" do
        user = FactoryGirl.create(:user)
        stop = FactoryGirl.create(:stop)
        expect {
          put :update,id: stop, stop: {description: stop['description'], serial: stop['serial']+1}, authentication_token: user.authentication_token
        }.not_to change(stop, :serial)
        response.status.should eq(403)
      end
    end
  end

  describe "DELETE #destroy" do
    context "logged in as admin" do
      context "with valid parameters" do
        it "should return a copy of the stop that was deleted" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          stop = FactoryGirl.create(:stop)
          delete :destroy, id: stop, authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('stop')
        end
        it "stop returned should only include serial, id, description and location" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          stop = FactoryGirl.create(:stop)
          delete :destroy, id: stop, authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['stop'].keys.count.should eq(4)
          body['stop'].should include('serial')
          body['stop'].should include('id')
          body['stop'].should include('description')
          body['stop'].should include('location')
        end
        it "stop location returned should only include latitude and longitude" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          stop = FactoryGirl.create(:stop)
          delete :destroy, id: stop, authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['stop']['location'].keys.count.should eq(2)
          body['stop']['location'].should include('latitude')
          body['stop']['location'].should include('longitude')
        end
        it "should remove the stop from the database" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          stop = FactoryGirl.create(:stop)
          expect {
            delete :destroy, id: stop, authentication_token: user.authentication_token
          }.to change(Stop, :count).by(-1)
        end
        it "should remove the stop's location from the database" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          stop = FactoryGirl.create(:stop)
          expect {
            delete :destroy, id: stop, authentication_token: user.authentication_token
          }.to change(Location, :count).by(-1)
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found it the stop id does not exist" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          if Stop.exists?(5)
            Stop.find(5).destroy
          end
          delete :destroy, id: 5, authentication_token: user.authentication_token
          response.status.should eq(404)
        end
      end
    end
    context "not logged in as admin" do
      it "should return 403 forbidden" do
        user = FactoryGirl.create(:user)
        stop = FactoryGirl.create(:stop)
        delete :destroy, id: stop, authentication_token: user.authentication_token
        response.status.should eq(403)
      end
      it "should not modify the database" do
        user = FactoryGirl.create(:user)
        stop = FactoryGirl.create(:stop)
        expect {
          delete :destroy, id: stop, authentication_token: user.authentication_token
        }.to_not change(Stop, :count)
      end
    end
  end

end
