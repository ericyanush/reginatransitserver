require 'spec_helper'

describe API::V1::RoutesController do
  describe "GET #index"  do
    it "should return a list of all routes" do
      FactoryGirl.create(:route)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body.should include('routes')
      body['routes'].count eq (Route.count)
    end
    it "should return each route with only id, description, list of buses, list of schedules, and list of stops" do
      route = FactoryGirl.create(:route)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body.should include('routes')
      body['routes'][0].keys.count.should eq(5)
      body['routes'][0].keys.should include('id')
      body['routes'][0].keys.should include('description')
      body['routes'][0].keys.should include('bus_ids')
      body['routes'][0]['bus_ids'].count.should eq(route.buses.count)
      body['routes'][0].keys.should include('schedule_ids')
      body['routes'][0]['schedule_ids'].count.should eq(route.schedules.count)
      body['routes'][0].keys.should include('stop_ids')
      body['routes'][0]['stop_ids'].count.should eq(route.stops.count)
    end
    it "should only embed bus ids" do
      route = FactoryGirl.create(:route)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body['routes'][0]['bus_ids'].kind_of?(Array).should eq(true)
    end
    it "should only embed schedule_ids" do
      route = FactoryGirl.create(:route)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body['routes'][0]['schedule_ids'].kind_of?(Array).should eq(true)
    end
    it "should only embed stop_ids" do
      route = FactoryGirl.create(:route)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body['routes'][0]['stop_ids'].kind_of?(Array).should eq(true)
    end
  end

  describe "POST #create" do
    context "logged in as admin user" do
      context "with valid parameters" do
        it "should create a new route" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.attributes_for(:route)
          expect {
            post :create, :format => :json, route: route,
                 authentication_token: user.authentication_token
          }.to change(Route, :count).by(1)
          response.status.should eq(201)
        end
        it "should return the route json object" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.attributes_for(:route)
          post :create, :format => :json, route: route,
               authentication_token: user.authentication_token
          response.status.should eq(201)
          body = JSON.parse(response.body)
          body.should include('route')
        end
        it "should return each route with only id, description, list of buses, list of schedules, and list of stops" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.attributes_for(:route)
          post :create, :format => :json, route: route,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body.should include('route')
          body['route'].keys.count.should eq(5)
          body['route'].keys.should include('id')
          body['route'].keys.should include('description')
          body['route'].keys.should include('bus_ids')
          body['route'].keys.should include('schedule_ids')
          body['route'].keys.should include('stop_ids')
        end
        it "should only embed bus ids" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.attributes_for(:route)
          post :create, :format => :json, route: route,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['route']['bus_ids'].kind_of?(Array).should eq(true)
        end
        it "should only embed schedule_ids" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.attributes_for(:route)
          post :create, :format => :json, route: route,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['route']['schedule_ids'].kind_of?(Array).should eq(true)
        end
        it "should only embed stop_ids" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.attributes_for(:route)
          post :create, :format => :json, route: route,
               authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['route']['stop_ids'].kind_of?(Array).should eq(true)
        end
      end
      context "with invalid parameters" do
        it "should not create a route object" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.attributes_for(:route)
          expect {
            post :create, :format => :json, route: { description: nil },
                 authentication_token: user.authentication_token
          }.to_not change(route, :count)
        end
        it "should return a json object with errors" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.attributes_for(:route)
          post :create, :format => :json, route: { description: nil },
               authentication_token: user.authentication_token
          body = JSON.parse(response.body)
          body.should include('errors')
        end
      end
    end
    context "not logged in as admin" do
      it "returns unauthorized if no user is logged in (auth token)" do
        route = FactoryGirl.attributes_for(:route)
        post :create, :format => :json, route: route
        response.status.should eq(401)
      end
      it "returns forbidden if the user logged in is not admin" do
        user = FactoryGirl.create(:user)
        route = FactoryGirl.attributes_for(:route)
        post :create, :format => :json, route: route,
             authentication_token: user.authentication_token
        response.status.should eq(403)
      end
      it "does not create a schedule" do
        user = FactoryGirl.create(:user)
        route = FactoryGirl.attributes_for(:route)
        expect {
          post :create, :format => :json, route: route,
               authentication_token: user.authentication_token
        }.to_not change(Route, :count)
        response.status.should eq(403)
      end
    end
  end

  describe "GET #show" do
    context "with valid id"  do
      it "returns a json object of the route" do
        route = FactoryGirl.create(:route)
        get :show, id: route.id, :format => :json
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('route')
      end
      it "should return each route with only id, description, list of buses, list of schedules, and list of stops" do
        route = FactoryGirl.create(:route)
        get :show, :format => :json, id: route.id
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('route')
        body['route'].keys.count.should eq(5)
        body['route'].keys.should include('id')
        body['route'].keys.should include('description')
        body['route'].keys.should include('bus_ids')
        body['route'].keys.should include('schedule_ids')
        body['route'].keys.should include('stop_ids')
      end
      it "should only embed bus ids" do
        route = FactoryGirl.create(:route)
        get :show, :format => :json, id: route.id
        response.should be_success
        body = JSON.parse(response.body)
        body['route']['bus_ids'].kind_of?(Array).should eq(true)
      end
      it "should only embed schedule_ids" do
        route = FactoryGirl.create(:route)
        get :show, :format => :json, id: route.id
        response.should be_success
        body = JSON.parse(response.body)
        body['route']['schedule_ids'].kind_of?(Array).should eq(true)
      end
      it "should only embed stop_ids" do
        route = FactoryGirl.create(:route)
        get :show, :format => :json, id: route.id
        response.should be_success
        body = JSON.parse(response.body)
        body['route']['stop_ids'].kind_of?(Array).should eq(true)
      end
    end
    context "with invalid id"  do
      it "should return a not found (404)" do
        if Route.exists?(5)
          Route.find(5).destroy
        end
        get :show, id: 5
        response.status.should eq(404)
      end
    end
  end

  describe "PUT #update" do
    context "logged in as admin" do
      context "with valid parameters" do
        it "should return an updated json schedule object" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          put :update, id: route.id, :format => :json, route: { description: "test update" },
              authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('route')
          body['route']['description'].should eq("test update")
        end
        it "should only return id, description, a list of buses, a list of schedules and a list of stops" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          put :update, id: route.id, :format => :json, route: { description: "test update" },
              authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('route')
          body['route'].keys.count.should eq(5)
          body['route'].should include('id')
          body['route'].should include('description')
          body['route'].should include('bus_ids')
          body['route'].should include('schedule_ids')
          body['route'].should include('stop_ids')
        end
        it "should only embed bus ids" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          put :update, id: route.id, :format => :json, route: { description: "test update" },
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['route']['bus_ids'].kind_of?(Array).should eq(true)
        end
        it "should only embed schedule_ids" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          put :update, id: route.id, :format => :json, route: { description: "test update" },
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['route']['schedule_ids'].kind_of?(Array).should eq(true)
        end
        it "should only embed stop_ids" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          put :update, id: route.id, :format => :json, route: { description: "test update" },
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['route']['stop_ids'].kind_of?(Array).should eq(true)
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found if the schedule id does not exist" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          if Route.exists?(5)
            Route.find(5).destroy
          end
          put :update, id: 5, route: { description: "test" }, authentication_token: user.authentication_token
          response.status.should eq(404)
        end
        it "should return errors when invalid attributes are supplied" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          put :update, id: route.id, route: { description: nil }, authentication_token: user.authentication_token
          response.status.should eq(400)
          body = JSON.parse(response.body)
          body.should include('errors')
        end
      end
    end
    context "not logged in as admin" do
      it "should return 403 forbiden" do
        user = FactoryGirl.create(:user)
        route = FactoryGirl.create(:route)
        expect {
          put :update, id: route.id, :format => :json, route: { description: "should not change" },
              authentication_token: user.authentication_token
        }.to_not change(route, :description)
        response.status.should eq(403)
      end
    end
  end

  describe "DELETE #destroy" do
    context "logged in as admin" do
      context "with valid parameters" do
        it "should return a copy of the route that was deleted" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          delete :destroy, id: route, authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('route')
        end
        it "should only return id, description, a list of buses, a list of schedules and a list of stops" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          delete :destroy, id: route.id, :format => :json,
              authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('route')
          body['route'].keys.count.should eq(5)
          body['route'].should include('id')
          body['route'].should include('description')
          body['route'].should include('bus_ids')
          body['route'].should include('schedule_ids')
          body['route'].should include('stop_ids')
        end
        it "should only embed bus ids" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          delete :destroy, id: route.id, :format => :json,
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['route']['bus_ids'].kind_of?(Array).should eq(true)
        end
        it "should only embed schedule_ids" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          delete :destroy, id: route.id, :format => :json,
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['route']['schedule_ids'].kind_of?(Array).should eq(true)
        end
        it "should only embed stop_ids" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          delete :destroy, id: route.id, :format => :json,
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['route']['stop_ids'].kind_of?(Array).should eq(true)
        end
        it "should remove the route from the database" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          expect {
            delete :destroy, id: route, authentication_token: user.authentication_token
          }.to change(Route, :count).by(-1)
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found it the stop id does not exist" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          if Route.exists?(5)
            Route.find(5).destroy
          end
          delete :destroy, id: 5, authentication_token: user.authentication_token
          response.status.should eq(404)
        end
      end
    end
    context "not logged in as admin" do
      it "should return 403 forbidden" do
        user = FactoryGirl.create(:user)
        route = FactoryGirl.create(:route)
        delete :destroy, id: route, authentication_token: user.authentication_token
        response.status.should eq(403)
      end
      it "should not modify the database" do
        user = FactoryGirl.create(:user)
        route = FactoryGirl.create(:route)
        expect {
          delete :destroy, id: route, authentication_token: user.authentication_token
        }.to_not change(Route, :count)
      end
    end
  end
end
