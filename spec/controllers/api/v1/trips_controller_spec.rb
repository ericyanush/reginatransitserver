require 'spec_helper'

describe API::V1::TripsController do
  describe "GET #index"  do
    context "logged in user" do
      it "should return a list of all trips for the logged in user" do
        trip = FactoryGirl.create(:trip)
        user = trip.user
        get :index, :format => :json,
            authentication_token: user.authentication_token
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('trips')
        body['trips'].count eq (Trip.where(user_id: user).count)
      end
      it "should return each trip only id, duration, start time, and lists of buses, routes, and stops" do
        trip = FactoryGirl.create(:trip)
        user = trip.user
        get :index, :format => :json,
            authentication_token: user.authentication_token
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('trips')
        body['trips'][0].keys.count.should eq(6)
        body['trips'][0].keys.should include('id')
        body['trips'][0].keys.should include('duration')
        body['trips'][0].keys.should include('start_time')
        body['trips'][0].keys.should include('bus_ids')
        body['trips'][0].keys.should include('route_ids')
        body['trips'][0].keys.should include('stop_ids')
      end
      it "should embed bus_ids" do
        trip = FactoryGirl.create(:trip)
        user = trip.user
        get :index, :format => :json,
            authentication_token: user.authentication_token
        response.should be_success
        body = JSON.parse(response.body)
        body['trips'][0]['bus_ids'].kind_of?(Array).should eq(true)
      end
      it "should embed route_ids" do
        trip = FactoryGirl.create(:trip)
        user = trip.user
        get :index, :format => :json,
            authentication_token: user.authentication_token
        response.should be_success
        body = JSON.parse(response.body)
        body['trips'][0]['route_ids'].kind_of?(Array).should eq(true)
      end
      it "should embed stop_ids" do
        trip = FactoryGirl.create(:trip)
        user = trip.user
        get :index, :format => :json,
            authentication_token: user.authentication_token
        response.should be_success
        body = JSON.parse(response.body)
        body['trips'][0]['route_ids'].kind_of?(Array).should eq(true)
      end
    end
    context "not logged in" do
      it "should return a 401 unauthorized" do
        trip = FactoryGirl.create(:trip)
        get :index, :format => :json
        response.status.should eq(401)
        body = JSON.parse(response.body)
        body.should include('errors')
      end
    end
  end

  describe "POST #create" do
    context "logged in user" do
      context "with valid parameters" do
        it "should create a new trip" do
          user = FactoryGirl.create(:user)
          trip = FactoryGirl.attributes_for(:trip)
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          expect {
            post :create, :format => :json, trip: {duration: trip[:duration], start_time:trip[:start_time],
                                                   stop_ids:[stop.id], bus_ids:[bus.id], route_ids: [route.id]},
                 authentication_token: user.authentication_token
          }.to change(Trip, :count).by(1)
          response.status.should eq(201)
        end
        it "should return the Trip json object" do
          user = FactoryGirl.create(:user)
          trip = FactoryGirl.attributes_for(:trip)
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          post :create, :format => :json, trip: {duration: trip[:duration], start_time:trip[:start_time],
                                                   stop_ids:[stop.id], bus_ids:[bus.id], route_ids: [route.id]},
                 authentication_token: user.authentication_token
          response.status.should eq(201)
          body = JSON.parse(response.body)
          body.should include('trip')
        end
        it "should return each trip only id, duration, start time, and lists of buses, routes, and stops" do
          user = FactoryGirl.create(:user)
          trip = FactoryGirl.attributes_for(:trip)
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          post :create, :format => :json, trip: {duration: trip[:duration], start_time:trip[:start_time],
                                                 stop_ids:[stop.id], bus_ids:[bus.id], route_ids: [route.id]},
               authentication_token: user.authentication_token
          response.status.should eq(201)
          body = JSON.parse(response.body)
          body.should include('trip')
          body['trip'].keys.count.should eq(6)
          body['trip'].keys.should include('id')
          body['trip'].keys.should include('duration')
          body['trip'].keys.should include('start_time')
          body['trip'].keys.should include('bus_ids')
          body['trip'].keys.should include('route_ids')
          body['trip'].keys.should include('stop_ids')
        end
        it "should embed bus_ids" do
          user = FactoryGirl.create(:user)
          trip = FactoryGirl.attributes_for(:trip)
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          post :create, :format => :json, trip: {duration: trip[:duration], start_time:trip[:start_time],
                                                 stop_ids:[stop.id], bus_ids:[bus.id], route_ids: [route.id]},
               authentication_token: user.authentication_token
          response.status.should eq(201)
          body = JSON.parse(response.body)
          body['trip']['bus_ids'].kind_of?(Array).should eq(true)
        end
        it "should embed route_ids" do
          user = FactoryGirl.create(:user)
          trip = FactoryGirl.attributes_for(:trip)
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          post :create, :format => :json, trip: {duration: trip[:duration], start_time:trip[:start_time],
                                                 stop_ids:[stop.id], bus_ids:[bus.id], route_ids: [route.id]},
               authentication_token: user.authentication_token
          response.status.should eq(201)
          body = JSON.parse(response.body)
          body['trip']['route_ids'].kind_of?(Array).should eq(true)
        end
        it "should embed stop_ids" do
          user = FactoryGirl.create(:user)
          trip = FactoryGirl.attributes_for(:trip)
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          post :create, :format => :json, trip: {duration: trip[:duration], start_time:trip[:start_time],
                                                 stop_ids:[stop.id], bus_ids:[bus.id], route_ids: [route.id]},
               authentication_token: user.authentication_token
          response.status.should eq(201)
          body = JSON.parse(response.body)
          body['trip']['route_ids'].kind_of?(Array).should eq(true)
        end
      end
      context "with invalid parameters" do
        it "should not create a device object" do
          user = FactoryGirl.create(:user)
          expect {
            post :create, :format => :json, trip: {duration: "string", start_time: nil,
                                                 stop_ids:"strings", bus_ids:"test", route_ids:"broken"},
               authentication_token: user.authentication_token
          }.to_not change(Trip, :count)
          response.status.should eq(400)
        end
        it "should return a json object with errors" do
          user = FactoryGirl.create(:user)
          post :create, :format => :json, trip: {duration: "string", start_time: nil,
                                                   stop_ids:"strings", bus_ids:"test", route_ids:"broken"},
                 authentication_token: user.authentication_token
          response.status.should eq(400)
          body = JSON.parse(response.body)
          body.should include('errors')
        end
      end
    end
    context "not logged in" do
      it "returns unauthorized if no user is logged in (auth token)" do
        post :create, :format => :json, trip: {duration: "string", start_time: nil,
                                               stop_ids:"strings", bus_ids:"test", route_ids:"broken"}
        response.status.should eq(401)
      end
      it "does not create a trip" do
        expect {
          post :create, :format => :json, trip: {duration: "string", start_time: nil,
                                               stop_ids:"strings", bus_ids:"test", route_ids:"broken"}
        }.to_not change(Trip, :count)
        response.status.should eq(401)
      end
    end
  end

  describe "GET #show" do
    context "logged in" do
      context "with valid id"  do
        it "returns a json object of the trip" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          get :show, :format => :json, id: trip.id,
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body.should include('trip')
        end
        it "should return each trip only id, duration, start time, and lists of buses, routes, and stops" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          get :show, id: trip.id, :format => :json,
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body.should include('trip')
          body['trip'].keys.count.should eq(6)
          body['trip'].keys.should include('id')
          body['trip'].keys.should include('duration')
          body['trip'].keys.should include('start_time')
          body['trip'].keys.should include('bus_ids')
          body['trip'].keys.should include('route_ids')
          body['trip'].keys.should include('stop_ids')
        end
        it "should embed bus_ids" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          get :show, id: trip.id, :format => :json,
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['trip']['bus_ids'].kind_of?(Array).should eq(true)
        end
        it "should embed route_ids" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          get :show, id: trip.id, :format => :json,
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['trip']['route_ids'].kind_of?(Array).should eq(true)
        end
        it "should embed stop_ids" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          get :show, id: trip.id, :format => :json,
              authentication_token: user.authentication_token
          response.should be_success
          body = JSON.parse(response.body)
          body['trip']['route_ids'].kind_of?(Array).should eq(true)
        end
      end
      context "with invalid id"  do
        it "should return a not found (404)" do
          if Trip.exists?(5)
            Trip.find(5).destroy
          end
          user = FactoryGirl.create(:user)
          get :show, id: 5,
              authentication_token: user.authentication_token
          response.status.should eq(404)
        end
      end
    end
    context "not logged in" do
      it "should return 401 not authorized" do
        trip = FactoryGirl.create(:trip)
        get :show, :format => :json, id: trip.id
        response.status.should eq(401)
      end
      it "should return a 404 not found if the trip does not match the user" do
        trip = FactoryGirl.create(:trip)
        user = FactoryGirl.create(:user)
        get :show,  :format => :json, id: trip.id,
            authentication_token: user.authentication_token
        response.status.should eq(404)
      end
    end
  end

  describe "PUT #update" do
    context "logged in user" do
      context "with valid parameters" do
        it "should return an updated json trip object" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          put :update, id: trip.id, :format => :json, trip: { duration: 8000,
                                                             route_ids: [route.id], bus_ids:[bus.id],
                                                             stop_ids:[stop.id]},
              authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('trip')
          body['trip']['duration'].should eq (8000)
        end
        it "should return each trip only id, duration, start time, and lists of buses, routes, and stops" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          put :update, id: trip.id, :format => :json, trip: {duration: 1111, start_time:Time.now,
                                                 stop_ids:[stop.id], bus_ids:[bus.id], route_ids: [route.id]},
               authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('trip')
          body['trip'].keys.count.should eq(6)
          body['trip'].keys.should include('id')
          body['trip'].keys.should include('duration')
          body['trip'].keys.should include('start_time')
          body['trip'].keys.should include('bus_ids')
          body['trip'].keys.should include('route_ids')
          body['trip'].keys.should include('stop_ids')
        end
        it "should embed bus_ids" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          post :update, id:trip.id, :format => :json, trip: {duration: 1234, start_time:Time.now,
                                                 stop_ids:[stop.id], bus_ids:[bus.id], route_ids: [route.id]},
               authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['trip']['bus_ids'].kind_of?(Array).should eq(true)
        end
        it "should embed route_ids" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          post :update, id:trip.id, :format => :json, trip: {duration: 1234, start_time:Time.now,
                                                 stop_ids:[stop.id], bus_ids:[bus.id], route_ids: [route.id]},
               authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['trip']['route_ids'].kind_of?(Array).should eq(true)
        end
        it "should embed stop_ids" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          stop = FactoryGirl.create(:stop)
          route = FactoryGirl.create(:route)
          bus = FactoryGirl.create(:bus)
          post :update, id:trip.id, :format => :json, trip: {duration: 3456, start_time:Time.now,
                                                 stop_ids:[stop.id], bus_ids:[bus.id], route_ids: [route.id]},
               authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['trip']['route_ids'].kind_of?(Array).should eq(true)
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found if the device id does not exist" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          if Device.exists?(5)
            Device.find(5).destroy
          end
          put :update, id: 5, device: { token: "test token" },
              authentication_token: user.authentication_token
          response.status.should eq(404)
        end
        it "should return errors when invalid attributes are supplied" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          put :update, id: trip.id, trip: { start_time: nil },
              authentication_token: user.authentication_token
          response.status.should eq(400)
          body = JSON.parse(response.body)
          body.should include('errors')
        end
      end
    end
    context "not logged in" do
      it "should return 401 unauthorized" do
        device = FactoryGirl.create(:device)
        expect {
          put :update, id: device.id, :format => :json, device: { token: "test token" }
        }.to_not change(device, :token)
        response.status.should eq(401)
      end
      it "should return 404 not found when the device doesn't belong to the user" do
        device = FactoryGirl.create(:device)
        user = FactoryGirl.create(:user)
        expect {
          put :update, id: device.id, :format => :json, device: { token: "test token" },
              authentication_token: user.authentication_token
        }.to_not change(device, :token)
        response.status.should eq(404)
      end
    end
  end

  describe "DELETE #destroy" do
    context "logged in" do
      context "with valid parameters" do
        it "should return a copy of the trip that was deleted" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          delete :destroy, id: trip.id,
                 authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('trip')
        end
        it "should return each trip only id, duration, start time, and lists of buses, routes, and stops" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          delete :destroy, id: trip.id, :format => :json,
              authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('trip')
          body['trip'].keys.count.should eq(6)
          body['trip'].keys.should include('id')
          body['trip'].keys.should include('duration')
          body['trip'].keys.should include('start_time')
          body['trip'].keys.should include('bus_ids')
          body['trip'].keys.should include('route_ids')
          body['trip'].keys.should include('stop_ids')
        end
        it "should embed bus_ids" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          delete :destroy, id:trip.id, :format => :json,
               authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['trip']['bus_ids'].kind_of?(Array).should eq(true)
        end
        it "should embed route_ids" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          delete :destroy, id:trip.id, :format => :json,
               authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['trip']['route_ids'].kind_of?(Array).should eq(true)
        end
        it "should embed stop_ids" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          delete :destroy, id:trip.id, :format => :json,
               authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['trip']['route_ids'].kind_of?(Array).should eq(true)
        end
        it "should remove the trip from the database" do
          trip = FactoryGirl.create(:trip)
          user = trip.user
          expect {
            delete :destroy, id: trip,
                   authentication_token: user.authentication_token
          }.to change(Trip, :count).by(-1)
        end
        it "should remove all associated stop_trips" do
          stoptrip = FactoryGirl.create(:stop_trip)
          trip = stoptrip.trip
          user = trip.user
          expect {
            delete :destroy, id: trip,
                   authentication_token: user.authentication_token
          }.to change(StopTrip, :count).by(-1)
        end
        it "should remove all associated route_trips" do
          routetrip = FactoryGirl.create(:route_trip)
          trip = routetrip.trip
          user = trip.user
          expect {
            delete :destroy, id: trip,
                   authentication_token: user.authentication_token
          }.to change(RouteTrip, :count).by(-1)
        end
        it "should remove all associated bus_trips" do
          bustrip = FactoryGirl.create(:bus_trip)
          trip = bustrip.trip
          user = trip.user
          expect {
            delete :destroy, id: trip,
                authentication_token: user.authentication_token
          }.to change(BusTrip, :count).by(-1)
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found if the user does not have a trip with that id" do
          user = FactoryGirl.create(:user)
          if Trip.exists?(5)
            Trip.find(5).destroy
          end
          delete :destroy, id: 5,
                 authentication_token: user.authentication_token
          response.status.should eq(404)
        end
      end
    end
    context "not logged in" do
      it "should return 401 unauthorized" do
        trip = FactoryGirl.create(:trip)
        delete :destroy, id: trip
        response.status.should eq(401)
      end
      it "should not modify the database" do
        trip = FactoryGirl.create(:trip)
        expect {
          delete :destroy, id: trip
        }.to_not change(Trip, :count)
      end
      it "should return 404 not found if the user does not match the trip" do
        trip = FactoryGirl.create(:trip)
        user = FactoryGirl.create(:user)
        expect {
          delete :destroy, id: trip,
                 authentication_token: user.authentication_token
        }.to_not change(Trip, :count)
        response.status.should eq(404)
      end
    end
  end
end
