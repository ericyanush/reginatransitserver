require 'spec_helper'
require 'devise'

describe API::V1::RegistrationsController do

  describe "POST #create"  do
    context "with valid attributes" do
      it "creates a new user" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        expect {
          post :create, user: FactoryGirl.attributes_for(:user), :format => :json
        }.to change(User, :count).by(1)
      end
      it "returns json object with success and the user object (email)" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.attributes_for(:user)
        post :create, user: user, :format => :json
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('success')
        body['success'].should eq(true)
        body.should include('user')
        body['user']['email'].should eq(user[:email])
      end
      it "should not return a user with flags other than email" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        user = FactoryGirl.attributes_for(:user)
        post :create, user: user, :format => :json
        response.should be_success
        body = JSON.parse(response.body)
        body['user'].keys.count.should eq(1)
        body['user'].keys.should include('email')
      end
    end
    context "with invalid attributes" do
      it "returns json object with success:false and errors" do
        @request.env["devise.mapping"] = Devise.mappings[:user]
        post :create, :format => :json
        response.status.should eq(400)
        body = JSON.parse(response.body)
        body.should include('success')
        body['success'].should eq(false)
        body.should include('errors')
      end
    end
  end
end
