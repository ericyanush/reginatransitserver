require 'spec_helper'

describe API::V1::SchedulesController do

  describe "GET #index"  do
    it "should return a list of all schedules" do
      FactoryGirl.create(:schedule)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body.should include('schedules')
      body['schedules'].count eq (Schedule.count)
    end
    it "should return each schedule with only id, route_id, bus_id and list of timepoints" do
      FactoryGirl.create(:schedule)
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body.should include('schedules')
      body['schedules'][0].keys.count.should eq(4)
      body['schedules'][0].keys.should include('id')
      body['schedules'][0].keys.should include('route_id')
      body['schedules'][0].keys.should include('bus_id')
      body['schedules'][0].keys.should include('timepoints')
    end
    it "should return a timepoint with only day, hour, and minute" do
      schedtp = FactoryGirl.create(:schedule_timepoint)
      sched = schedtp.schedule
      get :index, :format => :json
      response.should be_success
      body = JSON.parse(response.body)
      body.should include('schedules')
      body['schedules'][0]['timepoints'].count.should eq(sched.timepoints.count)
      body['schedules'][0]['timepoints'][0].keys.count.should eq(3)
      body['schedules'][0]['timepoints'][0].should include('day')
      body['schedules'][0]['timepoints'][0].should include('hour')
      body['schedules'][0]['timepoints'][0].should include('minute')
    end
  end

  describe "POST #create" do
    context "logged in as admin user" do
      context "with valid parameters" do
        it "should create a new schedule" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          schedule = FactoryGirl.attributes_for(:schedule)
          timepoint = FactoryGirl.attributes_for(:timepoint)
          expect {
            post :create, :format => :json, schedule: { route_id: route.id, schedule_timepoints_attributes: [timepoint_attributes:timepoint]},
                 authentication_token: user.authentication_token
          }.to change(Schedule, :count).by(1)
        end
        it "should return the schedule json object" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          schedule = FactoryGirl.attributes_for(:schedule)
          timepoint = FactoryGirl.attributes_for(:timepoint)
          post :create, :format => :json, schedule: { route_id: route.id, schedule_timepoints_attributes: [timepoint_attributes:timepoint]},
                 authentication_token: user.authentication_token
          body = JSON.parse(response.body)
          body.should include('schedule')
        end
        it "should return schedule object with only id, route_id, bus_id, and timepoints" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          schedule = FactoryGirl.attributes_for(:schedule)
          timepoint = FactoryGirl.attributes_for(:timepoint)
          post :create, :format => :json, schedule: { route_id: route.id, schedule_timepoints_attributes: [timepoint_attributes:timepoint]},
               authentication_token: user.authentication_token
          body = JSON.parse(response.body)
          body['schedule'].keys.count.should eq(4)
          body['schedule'].keys.should include('id')
          body['schedule'].keys.should include('route_id')
          body['schedule'].keys.should include('bus_id')
          body['schedule'].keys.should include('timepoints')
        end
        it "should return the timepoints with only day, hour, and minute" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          schedule = FactoryGirl.attributes_for(:schedule)
          timepoint = FactoryGirl.attributes_for(:timepoint)
          post :create, :format => :json, schedule: { route_id: route.id, schedule_timepoints_attributes: [timepoint_attributes:timepoint]},
               authentication_token: user.authentication_token
          body = JSON.parse(response.body)
          body['schedule']['timepoints'][0].keys.count.should eq(3)
          body['schedule']['timepoints'][0].keys.should include('day')
          body['schedule']['timepoints'][0].keys.should include('hour')
          body['schedule']['timepoints'][0].keys.should include('minute')
        end
      end
      context "with invalid parameters" do
        it "should not create a stop object" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          schedule = FactoryGirl.attributes_for(:schedule)
          timepoint = FactoryGirl.attributes_for(:timepoint)
          expect {
            post :create, :format => :json, schedule: { schedule_timepoints_attributes: [timepoint_attributes:timepoint]},
                 authentication_token: user.authentication_token
          }.to_not change(Schedule, :count)
        end
        it "should return a json object with errors" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          schedule = FactoryGirl.attributes_for(:schedule)
          timepoint = FactoryGirl.attributes_for(:timepoint)
          post :create, :format => :json, schedule: { schedule_timepoints_attributes: [timepoint_attributes:timepoint]},
                 authentication_token: user.authentication_token
          body = JSON.parse(response.body)
          body.should include('errors')
        end
        it "should return a bad request status" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          schedule = FactoryGirl.attributes_for(:schedule)
          timepoint = FactoryGirl.attributes_for(:timepoint)
          post :create, :format => :json, schedule: { schedule_timepoints_attributes: [timepoint_attributes:timepoint]},
               authentication_token: user.authentication_token
          response.status.should eq(400)
        end
      end
    end
    context "not logged in as admin" do
      it "returns unauthorized if no user is logged in (auth token)" do
        route = FactoryGirl.create(:route)
        schedule = FactoryGirl.attributes_for(:schedule)
        timepoint = FactoryGirl.attributes_for(:timepoint)
        post :create, :format => :json, schedule: { schedule_timepoints_attributes: [timepoint_attributes:timepoint]}
        response.status.should eq(401)
      end
      it "returns forbidden if the user logged in is not admin" do
        user = FactoryGirl.create(:user)
        route = FactoryGirl.create(:route)
        schedule = FactoryGirl.attributes_for(:schedule)
        timepoint = FactoryGirl.attributes_for(:timepoint)
        post :create, :format => :json, schedule: { schedule_timepoints_attributes: [timepoint_attributes:timepoint]},
             authentication_token: user.authentication_token
        response.status.should eq(403)
      end
      it "does not create a schedule" do
        user = FactoryGirl.create(:user)
        route = FactoryGirl.create(:route)
        schedule = FactoryGirl.attributes_for(:schedule)
        timepoint = FactoryGirl.attributes_for(:timepoint)
        expect {
          post :create, :format => :json, schedule: { schedule_timepoints_attributes: [timepoint_attributes:timepoint]},
             authentication_token: user.authentication_token
        }.to_not change(Schedule, :count)
        response.status.should eq(403)
      end
    end
  end

  describe "GET #show" do
    context "with valid id"  do
      it "returns a json object of the schedule" do
        schedtp = FactoryGirl.create(:schedule_timepoint)
        schedule = schedtp.schedule
        get :show, id: schedule.id, :format => :json
        response.should be_success
        body = JSON.parse(response.body)
        body.should include('schedule')
      end
      it "should return each schedule with only id, route_id, bus_id and list of timepoints" do
        schedtp = FactoryGirl.create(:schedule_timepoint)
        schedule = schedtp.schedule
        get :show, id: schedule.id, :format => :json
        response.should be_success
        body = JSON.parse(response.body)
        body['schedule'].keys.count.should eq(4)
        body['schedule'].should include('id')
        body['schedule'].should include('route_id')
        body['schedule'].should include('bus_id')
        body['schedule'].should include('timepoints')
      end
      it "should only return timepoints with day, hour, and minute" do
        schedtp = FactoryGirl.create(:schedule_timepoint)
        schedule = schedtp.schedule
        get :show, id: schedule.id, :format => :json
        response.should be_success
        body = JSON.parse(response.body)
        body['schedule'].should include('timepoints')
        body['schedule']['timepoints'][0].keys.count.should eq(3)
        body['schedule']['timepoints'][0].should include('day')
        body['schedule']['timepoints'][0].should include('hour')
        body['schedule']['timepoints'][0].should include('minute')
      end
    end
    context "with invalid id"  do
      it "should return a not found (404)" do
        if Schedule.exists?(5)
          Schedule.find(5).destroy
        end
        get :show, id: 5
        response.status.should eq(404)
      end
    end
  end

  describe "PUT #update" do
    context "logged in as admin" do
      context "with valid parameters" do
        it "should return an updated json schedule object" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          timepoint = FactoryGirl.create(:schedule_timepoint)
          schedule = timepoint.schedule
          bus = FactoryGirl.create(:bus)
          put :update, id: schedule.id, :format => :json, schedule: { bus_id:bus.id },
                 authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('schedule')
          body['schedule']['bus_id'].should eq(bus.id)
        end
        it "should only return id, route_id, bus_id and timepoints" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          timepoint = FactoryGirl.create(:schedule_timepoint)
          schedule = timepoint.schedule
          bus = FactoryGirl.create(:bus)
          put :update, id: schedule.id, :format => :json, schedule: { bus_id:bus.id },
              authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('schedule')
          body['schedule'].keys.count.should eq(4)
          body['schedule'].should include('id')
          body['schedule'].should include('route_id')
          body['schedule'].should include('bus_id')
          body['schedule'].should include('timepoints')
        end
        it "should only return day, hour, and minte for timepoints" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          timepoint = FactoryGirl.create(:schedule_timepoint)
          schedule = timepoint.schedule
          bus = FactoryGirl.create(:bus)
          put :update, id: schedule.id, :format => :json, schedule: { bus_id:bus.id },
              authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['schedule'].should include('timepoints')
          body['schedule']['timepoints'][0].keys.count.should eq(3)
          body['schedule']['timepoints'][0].should include('day')
          body['schedule']['timepoints'][0].should include('hour')
          body['schedule']['timepoints'][0].should include('minute')
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found if the schedule id does not exist" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          if Schedule.exists?(5)
            Schedule.find(5).destroy
          end
          put :update, id: 5, schedule: {route_id: 5, bus_id: "test"}, authentication_token: user.authentication_token
          response.status.should eq(404)
        end
        it "should return errors when invalid attributes are supplied" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          route = FactoryGirl.create(:route)
          timepoint = FactoryGirl.create(:schedule_timepoint)
          schedule = timepoint.schedule
          bus = FactoryGirl.create(:bus)
          put :update, id: schedule, schedule: {route_id: "garbage", bus_id: nil,
                                                schedule_timepoints_attributes: [timepoint_attributes: {day: 66, hour: -5, minute: "abc"}]}, authentication_token: user.authentication_token
          response.status.should eq(400)
          body = JSON.parse(response.body)
          body.should include('errors')
        end
      end
    end
    context "not logged in as admin" do
      it "should return 403 forbiden" do
        user = FactoryGirl.create(:user)
        route = FactoryGirl.create(:route)
        timepoint = FactoryGirl.create(:schedule_timepoint)
        schedule = timepoint.schedule
        bus = FactoryGirl.create(:bus)
        expect {
          put :update, id: schedule.id, :format => :json, schedule: { bus_id:bus.id },
            authentication_token: user.authentication_token
        }.to_not change(schedule, :bus_id)
        response.status.should eq(403)
      end
    end
  end

  describe "DELETE #destroy" do
    context "logged in as admin" do
      context "with valid parameters" do
        it "should return a copy of the schedule that was deleted" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          schedule = FactoryGirl.create(:schedule)
          delete :destroy, id: schedule, authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('schedule')
        end
        it "schedule returned should only include id, route_id, bus_id and timepoints" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          schedule = FactoryGirl.create(:schedule)
          delete :destroy, id: schedule, authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body.should include('schedule')
          body['schedule'].keys.count.should eq(4)
          body['schedule'].should include('id')
          body['schedule'].should include('route_id')
          body['schedule'].should include('bus_id')
          body['schedule'].should include('timepoints')
        end
        it "timepoints returned should only include day, hour, and minte" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          schedtp = FactoryGirl.create(:schedule_timepoint)
          schedule = schedtp.schedule
          delete :destroy, id: schedule, authentication_token: user.authentication_token
          response.status.should eq(200)
          body = JSON.parse(response.body)
          body['schedule'].should include('timepoints')
          body['schedule']['timepoints'][0].keys.count.should eq(3)
          body['schedule']['timepoints'][0].should include('day')
          body['schedule']['timepoints'][0].should include('hour')
          body['schedule']['timepoints'][0].should include('minute')
        end
        it "should remove the schedule from the database" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          schedule = FactoryGirl.create(:schedule)
          expect {
            delete :destroy, id: schedule, authentication_token: user.authentication_token
          }.to change(Schedule, :count).by(-1)
        end
        it "should remove the any related timepoints the database" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          schedtp = FactoryGirl.create(:schedule_timepoint)
          schedule = schedtp.schedule
          expect {
            delete :destroy, id: schedule, authentication_token: user.authentication_token
          }.to change(Timepoint, :count).by(-schedule.timepoints.count)
        end
        it "should remove the any related schedule_timepoints the database" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          schedtp = FactoryGirl.create(:schedule_timepoint)
          schedule = schedtp.schedule
          expect {
            delete :destroy, id: schedule, authentication_token: user.authentication_token
          }.to change(ScheduleTimepoint, :count).by(-schedule.schedule_timepoints.count)
        end
      end
      context "with invalid parameters" do
        it "should return a 404 not found it the stop id does not exist" do
          user = FactoryGirl.build(:user, is_admin: true)
          user.save
          if Schedule.exists?(5)
            Schedule.find(5).destroy
          end
          delete :destroy, id: 5, authentication_token: user.authentication_token
          response.status.should eq(404)
        end
      end
    end
    context "not logged in as admin" do
      it "should return 403 forbidden" do
        user = FactoryGirl.create(:user)
        schedule = FactoryGirl.create(:schedule)
        delete :destroy, id: schedule, authentication_token: user.authentication_token
        response.status.should eq(403)
      end
      it "should not modify the database" do
        user = FactoryGirl.create(:user)
        schedule = FactoryGirl.create(:schedule)
        expect {
          delete :destroy, id: schedule, authentication_token: user.authentication_token
        }.to_not change(Schedule, :count)
      end
    end
  end

end
