class Route < ActiveRecord::Base
  validates :description, presence: true
  validate :hasMatchingScheduleAndStops

  has_many :buses
  has_many :schedules
  has_many :route_stops
  has_many :stops, -> { order 'route_stops.stop_order' }, through: :route_stops
  has_many :route_trips
  has_many :trips, through: :route_trips

  def hasMatchingScheduleAndStops
   if schedules(true).present?
     schedules.each do |schedule|
       if schedule.timepoints.count != stops.count
         errors.add(:schedule, "must have the same number of times as stops in the route")
       end
     end
   end
  end
end
