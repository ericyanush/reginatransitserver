class BusTrip < ActiveRecord::Base
  validates :bus, presence: true
  validates :trip, presence: true
  validates :bus_order, presence: true, numericality: { greater_than_or_equal_to: 0 },
            uniqueness: { scope: :trip }

  belongs_to :trip
  belongs_to :bus
end
