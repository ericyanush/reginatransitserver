class Timepoint < ActiveRecord::Base
  validates :day, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 6 }
  validates :hour, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 23 }
  validates :minute, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 59 }
  belongs_to :event, polymorphic: true
end
