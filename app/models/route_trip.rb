class RouteTrip < ActiveRecord::Base
  validates :trip, presence: true
  validates :route, presence: true
  validates :route_order, presence: true, numericality: { greater_than_or_equal_to: 0 },
            uniqueness: { scope: :trip }

  belongs_to :trip
  belongs_to :route
end
