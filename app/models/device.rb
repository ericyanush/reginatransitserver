class Device < ActiveRecord::Base
  validates :token, presence: true
  validates :user, presence: true
  belongs_to :user
end
