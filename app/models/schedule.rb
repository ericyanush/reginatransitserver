class Schedule < ActiveRecord::Base
  validates :route, presence: true

  belongs_to :route
  belongs_to :bus
  has_many :schedule_timepoints, inverse_of: :schedule
  accepts_nested_attributes_for :schedule_timepoints
  has_many :timepoints, -> { order 'schedule_timepoints.time_order' }, through: :schedule_timepoints
end
