class RouteStop < ActiveRecord::Base
  validates :route, presence: true
  validates :stop, presence: true
  validates :stop_order, presence: true, numericality: { greater_than_or_equal_to: 0 },
            uniqueness: { scope: :route }

  belongs_to :route
  belongs_to :stop
end
