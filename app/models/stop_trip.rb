class StopTrip < ActiveRecord::Base
  validates :stop, presence: true
  validates :trip, presence: true
  validates :stop_order, presence: true, numericality: { greater_than_or_equal_to: 0 }, uniqueness: { scope: :trip }

  belongs_to :trip
  belongs_to :stop
end
