class Trip < ActiveRecord::Base
  validates :user, presence: true
  validates :start_time, presence: true
  validates :duration, presence: true, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 86400 }

  has_many :bus_trips, inverse_of: :trip, dependent: :destroy
  accepts_nested_attributes_for :bus_trips
  has_many :buses, ->{order 'bus_trips.bus_order'}, through: :bus_trips
  has_many :stop_trips, inverse_of: :trip, dependent: :destroy
  accepts_nested_attributes_for :stop_trips
  has_many :stops, ->{order 'stop_trips.stop_order'}, through: :stop_trips
  has_many :route_trips, inverse_of: :trip, dependent: :destroy
  accepts_nested_attributes_for :route_trips
  has_many :routes, ->{order 'route_trips.route_order'}, through: :route_trips
  belongs_to :user

end
