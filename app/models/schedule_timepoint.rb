class ScheduleTimepoint < ActiveRecord::Base
  validates :timepoint, presence: true
  validates :schedule, presence: true
  validates :time_order, presence: true, numericality: { greater_than_or_equal_to: 0 },
            uniqueness: { scope: :schedule }

  has_one :timepoint, as: :event
  accepts_nested_attributes_for :timepoint
  belongs_to :schedule
end
