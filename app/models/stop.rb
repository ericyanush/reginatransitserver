class Stop < ActiveRecord::Base
  validates :description, presence: true
  validates :serial, presence: true, numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 999999},
            uniqueness: true
  validates :location, presence: true

  has_one :location, as: :thing, :dependent => :destroy
  accepts_nested_attributes_for :location
  has_many :route_stops
  has_many :routes, through: :route_stops
  has_many :stop_trips
  has_many :trips, through: :stop_trips
end
