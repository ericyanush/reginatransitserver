class Bus < ActiveRecord::Base
  validates :unit_number, presence: true, numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 9999 },
            uniqueness: true

  has_one :location, as: :thing, dependent: :destroy
  accepts_nested_attributes_for :location
  has_one :schedule
  belongs_to :route
  has_many :bus_trips
  has_many :trips, through: :bus_trips

end
