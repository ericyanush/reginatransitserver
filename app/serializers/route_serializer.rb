class RouteSerializer < ActiveModel::Serializer
  attributes :id, :description
  has_many :stops
  has_many :schedules
  has_many :buses
  embed :ids
end
