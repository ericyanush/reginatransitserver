class ScheduleSerializer < ActiveModel::Serializer
  attributes :id
  has_one :route, embed: :ids
  has_one :bus, embed: :ids
  has_many :timepoints
end
