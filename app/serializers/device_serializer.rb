class DeviceSerializer < ActiveModel::Serializer
  attributes :id, :token
end
