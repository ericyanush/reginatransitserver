class BusSerializer < ActiveModel::Serializer
  attributes :id, :unit_number
  has_one :schedule, embed: :id
  has_one :route, embed: :id
  has_one :location
end
