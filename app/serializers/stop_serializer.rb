class StopSerializer < ActiveModel::Serializer
  attributes :id, :serial, :description
  has_one :location
end
