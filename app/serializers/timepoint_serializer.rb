class TimepointSerializer < ActiveModel::Serializer
  attributes :day, :hour, :minute
end
