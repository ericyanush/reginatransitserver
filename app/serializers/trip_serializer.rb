class TripSerializer < ActiveModel::Serializer
  attributes :id, :duration, :start_time
  has_many :buses, embed: :ids
  has_many :routes, embed: :ids
  has_many :stops, embed: :ids
end
