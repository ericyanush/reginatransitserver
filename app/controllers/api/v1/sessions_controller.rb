class API::V1::SessionsController < ApplicationController

  def create
    user = User.find_for_database_authentication(:email => params[:user][:email])

    if user && user.valid_password?(params[:user][:password])
      user.ensure_authentication_token!  # make sure the user has a token generated
      render :json => { :authentication_token => user.authentication_token,
                        :user => user.as_json(:only => :email) }, :status => :created
    else
      return invalid_login_attempt
    end
  end

  def destroy
    user = User.where(:authentication_token => params[:authentication_token]).first
    if user
      user.reset_authentication_token!
      render :json => { :message => ["Session deleted."],
        user: user.as_json(:only => :email)},  :success => true, :status => :ok
    else
      render :json => { :errors => ["Authentication token not found"], :success => false }, :success => false, :status => :unauthorized
    end
  end

  def invalid_login_attempt
    warden.custom_failure!
    render :json => { :errors => ["Invalid email or password."], :success => false },  :success => false, :status => :unauthorized
  end

end
