class API::V1::TripsController < ApplicationController
  before_filter :after_token_authentication

  def index
    @trips = Trip.where(:user => current_user)
    render json: @trips
  end

  def create
    if @trip = Trip.create(trip_params)
      @trip.user = current_user
      if @trip.save
        render json: @trip, status: :created
      else
        render json: { errors: @trip.errors }, status: :bad_request
      end
    end
  end

  def show
    if Trip.exists?(params[:id])
      @trip = Trip.find(params[:id])
      if @trip.user == current_user
        render json: @trip
      else
        render json: { errors: ["404 Not Found"] }, status: :not_found
      end
    else
      render json: { errors: ["404 Not Found"] }, status: :not_found
    end
  end

  def update
    if Trip.exists?(params[:id])
      @trip = Trip.find(params[:id])
      if @trip.user == current_user
        @trip.update_attributes(trip_params)
        if @trip.save
          return render json: @trip
        else
          return render json: { errors: @trip.errors }, status: :bad_request
        end
      end
    end
    render json: { errors: ["404 Not Found"] }, status: :not_found
  end

  def destroy
    if Trip.exists?(params[:id])
      @trip = Trip.find(params[:id])
      if @trip.user == current_user
        @trip.destroy
        return render json: @trip
      end
    end
    render json: { errors: ["404 Not Found"] }, status: :not_found
  end

  def after_token_authentication
    if params[:authentication_token].present?
      @user = User.find_by_authentication_token(params[:authentication_token])
      if @user
        sign_in @user
      else
        render :json => { errors: ["401 Forbidden"] }, status: :unauthorized
      end
    else
      render :json => { errors: ["401 Forbidden"] }, status: :unauthorized
    end
  end

  def trip_params
    if params[:trip]
      safe_params = params.require(:trip).permit(:duration, :start_time, :bus_ids => [], :route_ids =>[], :stop_ids =>[])
      #build bus_trips for each bus_id
      if safe_params[:bus_ids] and safe_params[:bus_ids].kind_of?(Array)
        bus_ids = safe_params[:bus_ids]
        safe_params.delete('bus_ids')
        safe_params[:bus_trips_attributes] = []
        bus_ids.each_with_index do |id, index|
          safe_params[:bus_trips_attributes][index] = { bus_id: id, bus_order: index }
        end
      else
        safe_params.delete('bus_ids')
      end
      #build route_trips for each route_id
      if safe_params[:route_ids] and safe_params[:route_ids].kind_of?(Array)
        route_ids = safe_params[:route_ids]
        safe_params.delete('route_ids')
        safe_params[:route_trips_attributes] = []
        route_ids.each_with_index do |id, index|
          safe_params[:route_trips_attributes][index] = { route_id: id, route_order: index }
        end
      else
        safe_params.delete('route_ids')
      end
      #build stop_trips for each stop_id
      if safe_params[:stop_ids] and safe_params[:stop_ids].kind_of?(Array)
        stop_ids = safe_params[:stop_ids]
        safe_params.delete('stop_ids')
        safe_params[:stop_trips_attributes] = []
        stop_ids.each_with_index do |id, index|
          safe_params[:stop_trips_attributes][index] = { stop_id:id, stop_order: index }
        end
      else
        safe_params.delete('stop_ids')
      end
      safe_params
    else
      nil
    end
  end
end
