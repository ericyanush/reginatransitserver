class API::V1::BusesController < ApplicationController
  before_filter :after_token_authentication, only: [:create, :update, :destroy]

  def index
    @buses = Bus.all
    render json: @buses
  end

  def create
    if current_user.is_admin?
      @bus = Bus.create(bus_params)
      if @bus.save
        render json: @bus, status: :created
      else
        render json:  { errors: @bus.errors }, status: :bad_request
      end
    else
      render json: { errors: ["403 Forbidden"] }, status: :forbidden
    end
  end

  def show
    if Bus.exists?(params[:id])
      @bus = Bus.find(params[:id])
      render json: @bus
    else
      render json: { errors: ["404 Not Found"] }, status: :not_found
    end
  end

  def update
    if current_user.is_admin?
      if Bus.exists?(params[:id])
        @bus = Bus.find(params[:id])
        if @bus.update_attributes(bus_params)
          render json: @bus
        else
          render json: { errors: @bus.errors }, status: :bad_request
        end
      else
        render json: { errors: ["404 Not Found"] }, status: :not_found
      end
    else
      render json: { errors: ["403 Forbidden"] }, status: :forbidden
    end
  end

  def destroy
    if current_user.is_admin?
      if Bus.exists?(params[:id])
        @bus = Bus.find(params[:id])
        @bus.destroy
        render json: @bus
      else
        render json: { errors: ["404 Not Found"] }, status: :not_found
      end
    else
      render json: { errors: ["403 Forbidden"] }, status: :forbidden
    end
  end

  def after_token_authentication
    if params[:authentication_token].present?
      @user = User.find_by_authentication_token(params[:authentication_token])
      if @user
        sign_in @user
      else
        render :json => { errors: ["401 Forbidden"] }, status: :unauthorized
      end
    else
      render :json => { errors: ["401 Forbidden"] }, status: :unauthorized
    end
  end

  def bus_params
    if params[:bus]
      params.require(:bus).permit(:unit_number, :schedule_id, :route_id, location_attributes:[:latitude, :longitude])
    else
      nil
    end
  end

end
