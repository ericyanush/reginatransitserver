class API::V1::SchedulesController < ApplicationController
  before_filter :after_token_authentication, :only => [:create, :update, :destroy]

  def index
    @schedules = Schedule.all
    render json: @schedules
  end

  def create
    if current_user.is_admin?
      @schedule = Schedule.create(schedule_params)
      if @schedule.save
        render json: @schedule
      else
        render :json => { errors: @schedule.errors }, status: :bad_request
      end
    else
      render :json => { errors: ["403 Forbidden"] }, status: :forbidden
    end
  end

  def show
    if Schedule.exists?(params[:id])
      @schedule = Schedule.find(params[:id])
      render json: @schedule
    else
      render :json => {errors: ["404 Not found"]}, :status => :not_found
    end
  end

  def update
    if current_user.is_admin?
      if Schedule.exists?(params[:id])
        @schedule = Schedule.find(params[:id])
        if @schedule.update_attributes(schedule_params)
          render json: @schedule, status: :ok
        else
          render :json => { errors: @schedule.errors }, :status => :bad_request
        end
      else
        render :json => { errors: ["404 Not Found"] }, :status => :not_found
      end
    else
      render :json => { errors: ["403 Forbidden"] }, :status => :forbidden
    end
  end

  def destroy
    if current_user.is_admin?
      if Schedule.exists?(params[:id])
        @schedule = Schedule.find(params[:id])
        @schedule.destroy
        @schedule.timepoints.each(&:destroy)
        @schedule.schedule_timepoints.each(&:destroy)
        render json: @schedule
      else
        render :json => { errors: ["404 Not Found"] }, :status => :not_found
      end
    else
      render :json => { errors: ["403 Forbidden"] }, :status => :forbidden
    end
  end

  def after_token_authentication
    if params[:authentication_token].present?
      @user = User.find_by_authentication_token(params[:authentication_token])
      if @user
        sign_in @user
      else
        render :json => { error: "401 Forbidden" }, status: :unauthorized
      end
    else
      render :json => { error: "401 Forbidden" }, status: :unauthorized
    end
  end

  def schedule_params
    if params[:schedule]
      safe_params = params.require(:schedule).permit(:route_id, :bus_id, :schedule_timepoints_attributes => [timepoint_attributes: [:day, :hour, :minute]])
      if safe_params[:schedule_timepoints_attributes]
        safe_params[:schedule_timepoints_attributes].each_with_index do |tp, index|
          tp[:time_order] = index
        end
      end
      safe_params
    else
      nil
    end
  end

end
