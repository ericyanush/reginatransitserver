class API::V1::DevicesController < ApplicationController
  before_filter :after_token_authentication


  def index
    @devices = Device.where(:user_id => current_user)
    render json: @devices
  end

  def create
    @device = Device.create(device_params)
    @device.user = current_user
    if @device.save
      render json: @device, status: :created
    else
      render json: { errors: @device.errors }, status: :bad_request
    end
  end

  def show
    if Device.exists?(params[:id])
      @device = Device.find(params[:id])
      if @device.user == current_user
        return render json: @device
      end
    end
    render json: { errors: ["404 Not Found"] }, status: :not_found
  end

  def update
    if Device.exists?(params[:id])
      @device = Device.find(params[:id])
      if @device.user == current_user
        if @device.update_attributes(device_params)
          render json: @device
        else
          render json: { errors: @device.errors }, status: :bad_request
        end
      else
        render json: { errors: ["404 Not Found"] }, status: :not_found
      end
    else
      render json: { errors: ["404 Not Found"] }, status: :not_found
    end
  end

  def destroy
    if Device.exists?(params[:id])
      @device = Device.find(params[:id])
      if @device.user == current_user
        @device.destroy
        render json: @device
      else
        render json: { errors: ["404 Not Found"] }, status: :not_found
      end
    else
      render json: { errors: ["404 Not Found"] }, status: :not_found
    end
  end

  def after_token_authentication
    if params[:authentication_token].present?
      @user = User.find_by_authentication_token(params[:authentication_token])
      if @user
        sign_in @user
      else
        render :json => { errors: ["401 Forbidden"] }, status: :unauthorized
      end
    else
      render :json => { errors: ["401 Forbidden"] }, status: :unauthorized
    end
  end

  def device_params
    if params[:device]
      params.require(:device).permit(:token)
    else
      nil
    end
  end

end
