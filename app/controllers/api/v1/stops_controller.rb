class API::V1::StopsController < ApplicationController
  before_filter :after_token_authentication, :only => [:create, :update, :destroy]

  def index
    @stops = Stop.all
    render json: @stops, status: :ok
  end

  def create
    if current_user.is_admin?
      @stop = Stop.create(stop_params)
      if @stop.save
        return render json: @stop, status: :created
      else
        return render :json => { errors: @stop.errors }, status: :bad_request
      end
    end
    render :json => { errors: ["403 Forbidden"] }, status: :forbidden
  end

  def show
    if Stop.exists?(params[:id])
      @stop = Stop.find(params[:id])
      render json: @stop, status: :ok
    else
      render :json => {errors: ["404 Not found"] }, :status => :not_found
    end
  end

  def update
    if current_user.is_admin?
      if Stop.exists?(params[:id])
        @stop = Stop.find(params[:id])
        if @stop.update_attributes(stop_params)
          render json: @stop, status: :ok
        else
          render :json => { errors: @stop.errors }, :status => :bad_request
        end
      else
        render :json => { errors: ["404 Not Found"] }, :status => :not_found
      end
    else
      render :json => { errors: ["403 Forbidden"] }, :status => :forbidden
    end
  end

  def destroy
    if current_user.is_admin?
      if Stop.exists?(params[:id])
        @stop = Stop.find(params[:id])
        if @stop.destroy
          render json: @stop, status: :ok
        end
      else
        render :json => { errors: ["404 not found"] }, :status => :not_found
      end
    else
      render :json => { errors: ["403 Forbiden"] }, :status => :forbidden
    end
  end

  def after_token_authentication
    if params[:authentication_token].present?
      @user = User.find_by_authentication_token(params[:authentication_token])
      if @user
        sign_in @user
      else
        render :json => { errors: ["401 Forbidden"] }, status: :unauthorized
      end
    else
      render :json => { errors: ["401 Forbidden"] }, status: :unauthorized
    end
  end

  private
    def stop_params
      if params[:stop]
        params.require(:stop).permit(:serial, :description, location_attributes:[:latitude, :longitude])
      else
        nil
      end
    end

end
