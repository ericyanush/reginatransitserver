class API::V1::RegistrationsController < Devise::RegistrationsController
  respond_to :json
  skip_before_filter :verify_authenticity_token, :only => :create

  def cancel
    render :file => "#{Rails.root}/public/404.html", :status => 404
  end

  def create
    build_resource sign_up_params
    if resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_up(resource_name, resource)
        return render :json => {:success => true,
          :user =>resource.as_json(only: [:email])}
      else
        set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
        expire_session_data_after_sign_in!
        return render :json => {:success => true}
      end
    else
      clean_up_passwords resource
      render :json => {:success => false, :errors => resource.errors.messages}, :status => 400
    end
  end

  def new
    render :file => "#{Rails.root}/public/404.html", :status => 404
  end

  def edit
    render :file => "#{Rails.root}/public/404.html", :status => 404
  end

  def update
    render :file => "#{Rails.root}/public/404.html", :status => 404
  end

  def destroy
    render :file => "#{Rails.root}/public/404.html", :status => 404
  end

end
