class API::V1::RoutesController < ApplicationController
  before_filter :after_token_authentication, :only => [:create, :update, :destroy]

  def index
    @routes = Route.all
    render json: @routes
  end

  def create
    if current_user.is_admin?
      @route = Route.create(route_params)
      if @route.save
        render json: @route, status: :created
      else
        render json:  { errors: ["400 Bad request"] }, status: :bad_request
      end
    else
      render json:  { errors: ["401 Forbidden"] }, status: :forbidden
    end
  end

  def show
    if Route.exists?(params[:id])
      @route = Route.find(params[:id])
      render json: @route, status: :ok
    else
      render json: { errors: ["404 Not Found"] }, status: :not_found
    end
  end

  def update
    if current_user.is_admin?
      if Route.exists?(params[:id])
        @route = Route.find(params[:id])
        if @route.update_attributes(route_params)
          render json: @route
        else
          render json: { errors: @route.errors }, status: :bad_request
        end
      else
        render json: { errors: ["404 Not Found"] }, status: :not_found
      end
    else
      render json: { errors: ["403 Forbidden"] }, status: :forbidden
    end
  end

  def destroy
    if current_user.is_admin?
      if Route.exists?(params[:id])
        @route = Route.find(params[:id])
        @route.destroy
        render json: @route, status: :ok
      else
        render json: { errors: ["404 Not Found"] }, status: :not_found
      end
    else
      render json: { errors: ["403 Forbidden"] }, status: :forbidden
    end
  end

  def after_token_authentication
    if params[:authentication_token].present?
      @user = User.find_by_authentication_token(params[:authentication_token])
      if @user
        sign_in @user
      else
        render :json => { errors: ["401 Unauthorized"] }, status: :unauthorized
      end
    else
      render :json => { errors: ["401 Unauthorized"] }, status: :unauthorized
    end
  end

  def route_params
    if params[:route]
      params.require(:route).permit(:description, :bus_ids, :schedule_ids, :stop_ids)
    else
      nil
    end
  end

end
